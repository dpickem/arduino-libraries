/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

// Include Pixel dependencies.
#include "device_group.h"

// Destructor.
DeviceGroup::~DeviceGroup() {
  // Delete all device mode pointers associated with the group.
  modes_.clear();
}

// Utility functions.
bool DeviceGroup::add_device_mode(DeviceMode* mode) {
  // Check if device mode is already registered.
  if (get_device_mode_by_name(mode->get_name()) == nullptr) {
    // Add device mode to mode map.
    modes_.add(mode);
    LOG_VALUE("Successfully added new device mode", mode->get_name());

    // Set current_mode_ if this is the first mode added.
    if (modes_.size() == 1) {
      current_mode_ = 0;
      LOG_VALUE("Set newly added device mode as current mode", mode->get_name());
    }

    return true;
  }

  LOG_VALUE("Failed to add already existing device mode", mode->get_name());
  return false;
}

/*! @brief Remove a device mode from the map of available device modes.
 *  
 *  The behavior of this function is as follows:
 *
 *  1. If the device mode with the specified mode does exist, it is removed,
 *     the current mode is set to the first available mode, and true is
 *     returned.
 *  2. If the device mode does not exist, this function returns false.
 */
bool DeviceGroup::remove_device_mode(String name) {
/*
  if (modes_.size() > 0) {
    for(std::vector<DeviceMode*>::iterator it = modes_.begin(); it != modes_.end(); ++it) {
      // Retrieve current device group pointer.
      DeviceMode* mode = *it;

      // If a group of requested name is found, delete it.
      if (mode->get_name() == name) {
        // Delete specified mode.
        modes_.erase(it);

        // Set current mode to first available mode.
        current_mode_ = 0;

        LOG_VALUE("Successfully deleted mode ", name);
        return true;
      }
    }

    LOG_VALUE("Failed to remove non-existant device mode", name);
  } else {
    LOG_VALUE("No device modes available. Failed to remove device mode", name);
  }

  return false;
*/
}

bool DeviceGroup::set_next_device_mode() {
  if (modes_.size() > 0) {
    current_mode_++;

    if (current_mode_ >= modes_.size()) {
      current_mode_ = 0;
      LOG("Setting next device mode rolled over from last to first mode.");
    }

    return true;
  } else {
    LOG("Failed to set next device mode. No device modes registered.");
  }

  return false;
}

bool DeviceGroup::set_previous_device_mode() {
  if (modes_.size() > 0) {
    current_mode_--;

    if (current_mode_ < 0) {
      current_mode_ = modes_.size() - 1;
      LOG("Setting previous device mode rolled over from first to last mode.");
    }

    return true;
  } else {
    LOG("Failed to set previous device mode. No device modes registered.");
  }

  return false;
}

bool DeviceGroup::set_current_device_mode_to_max() {
  if (modes_.size() > 0) {
    current_mode_ = modes_.size() - 1;
    LOG_VALUE("Set current device mode to max mode", modes_.get(current_mode_)->get_name());
    return true;
  }

  return false;
}

bool DeviceGroup::set_current_device_mode_to_min() {
  if (modes_.size() > 0) {
    current_mode_ = 0;
    LOG_VALUE("Set current device mode to min mode", modes_.get(current_mode_)->get_name());
    return true;
  }

  return false;
}

bool DeviceGroup::update_current_device_mode(float min_val,
                                             float max_val,
                                             float step) {
  if (modes_.size() > 0) {
    modes_.get(current_mode_)->update_device_range(min_val, max_val, step);
    return true;
  }

  return false;
}

void DeviceGroup::print_device_group_details() {
  Serial.println("----------------------------------------------");
  Serial.print("Device group name: ");
  Serial.println(name_);
  Serial.print("   Number of device modes: ");
  Serial.println(get_number_of_device_modes());
  Serial.print("   Current mode: ");
  Serial.print(modes_.get(current_mode_)->get_name());
  Serial.print(" (");
  Serial.print(current_mode_);
  Serial.println(")");

  // Print color information.
  Serial.print("   Color: ");
  Serial.println(color_);
  
  // Print mode information.
  Serial.println("   Modes: ");

  uint8_t mode_count = 0;
  for(int i = 0; i < modes_.size(); i++){
    Serial.print("    - Mode ");
    Serial.print(mode_count);
    Serial.print(": ");
    Serial.println(modes_.get(i)->get_name());
    mode_count++;
  }

  Serial.println("----------------------------------------------");
}

// Getter functions.
String DeviceGroup::get_name() {
  return name_;
}

DeviceMode* DeviceGroup::get_current_device_mode() {
  if (modes_.size() > 0) {
    return modes_.get(current_mode_);
  }

  return nullptr;
}

DeviceMode* DeviceGroup::get_device_mode_by_name(String name) {
  if (modes_.size() > 0) {
    // Find mode with specified name.
    for(int i = 0; i < modes_.size(); i++){
      if (modes_.get(i)->get_name() == name) {
        // LOG_VALUE("Found device mode with name", name);
        return modes_.get(i);
      }
    }

    LOG_VALUE("Failed to find device group", name);
  } else {
    LOG_VALUE("Failed to get device mode with name", name);
  }

  return nullptr;
}

DeviceMode* DeviceGroup::get_device_mode_by_index(uint8_t index) {
  if (modes_.size() > 0) {
    if (index >= 0 and index < modes_.size()) {
      return modes_.get(index);
    }

    LOG_VALUE("Failed to find device group because index is invalid", index);
  } else {
    LOG_VALUE("Failed to get device mode with index", index);
  }

  return nullptr;
}

int DeviceGroup::get_number_of_device_modes() {
  return modes_.size();
}

String DeviceGroup::get_color() {
  return color_;
}

// Setter functions.
void DeviceGroup::set_name(String name) {
  name_ = name;
}

void DeviceGroup::set_color(String color_name) {
  color_ = color_name;
}
