This is an abstraction library for the Atmel QTouch AT42QT2120 driver which
implements the delegate pattern.

The AT42QT2120 QTouch library can be found here:

  * https://bitbucket.org/dpickem/arduino-libraries/src/master/

This library is based on work by Appfruits, which can be found at:

  * http://www.appfruits.com/2015/12/little-helper/
  * https://github.com/appfruits/LittleHelper/blob/master/Firmware/TouchWheel.h
  * https://github.com/appfruits/LittleHelper/blob/master/Firmware/TouchWheel.cpp
