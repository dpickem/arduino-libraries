/*
 * This is a library for the Atmel QTouch AT42QT2120
 * But it should work with other QTouch Devices as well
 *
 * This software is part of the firmware running Little Helper. You may use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software 
 * but you must include this copyright notice and this permission in all copies or 
 * substantial portions of the Software.
 *
 * Appfruits invests time and resources to make this open source. Please support us
 * and the open-source hardware initiative.
 * 
 * Copyright 2014-2015 Phillip Schuster (@appfruits)
 * http://www.appfruits.com
 * 
 * MIT-License
 *
 * Modified 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 */

#undef TOUCH_WHEEL_DEBUG

#ifndef _TOUCHWHEEL_H_
#define _TOUCHWHEEL_H_

// Include AT42QT2120 driver.
#include "at42qt2120_qtouch.h"

// Define number of buttons
#define N_BUTTONS 4

// Define button directions.
typedef enum Direction {
  Left = 0,
  Top = 1,
  Right = 2,
  Bottom = 3
} Direction;

/*! @brief The implementation of the abstract delegate class
 *         for the AT42QT2120 QTouch sensor.
 */
class TouchWheel: public AT42QT2120_QTouch_Delegate {
  public:
    // Constructor.
    TouchWheel(AT42QT2120_QTouch* driver);

    // Setup and update function.
    void begin();
    void begin(uint8_t sda_pin, uint8_t scl_pin);
    void setup();
    void update();

    // Mode detection functions.
    bool isButtonReleased(Direction direction);
    bool isAnyButtonReleased();
    bool isSliding();

    // Functions for reading position and change in position
    // in slider/wheel mode.
    int8_t getSliderDelta();
    uint8_t getSliderPosition();

  private:
    // Class variables.
    bool      _sliding;
    bool      _touching;
    uint8_t   _sliderDelta;
    uint8_t   _sliderPos;
    uint32_t  _button[N_BUTTONS];
    uint32_t  _touchStart;
    AT42QT2120_QTouch* _driver;

    // Private class functions.
    void resetStates();
    void debugPrint(String function_name, uint32_t duration);
    virtual void onTouchLeftButtonPressed(uint32_t duration) override;
    virtual void onTouchTopButtonPressed(uint32_t duration) override;
    virtual void onTouchRightButtonPressed(uint32_t duration) override;
    virtual void onTouchBottomButtonPressed(uint32_t duration) override;
    virtual void onSlider(uint8_t sliderPos, int8_t sliderDelta) override;
    virtual void onTouchDown() override;
    virtual void onTouchUp(uint32_t duration) override;
};

#endif //_TOUCHWHEEL_H_
