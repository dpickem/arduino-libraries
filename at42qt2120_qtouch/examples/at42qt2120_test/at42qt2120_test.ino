/***************************************************************************
  This is a library for the AT42QT2120 capacitive touch sensor.

  This sensor uses I2C to communicate, at a minimum 2 pins are required
  to interface.

  Written by Daniel Pickem.

  MIT license, all text above must be included in any redistribution
 ***************************************************************************/

#include "at42qt2120_qtouch.h"
#include "touch_wheel.h"

// Instantiate QTouch sensor.
// NOTE: On the NodeMCU board, use pin D7 (i.e. pin 13).
//       On a regular Arduino Nano, use pin 3.
uint8_t changePin = 13;
AT42QT2120_QTouch qtouch(changePin);
TouchWheel wheel(&qtouch);

void setup() {
  // Open a serial connection for debugging.
  Serial.begin(115200);

  // Initialize QTouch sensor.
  if (!qtouch.begin()) {
    Serial.println("Could not find a valid AT42QT2120 QTouch sensor, check wiring!");
    while (1);
  }

  // Enable wheel.
  qtouch.enableWheel();
}

void loop() {
  Serial.print("pos: ");
  Serial.print(qtouch.sliderPosition());
  Serial.print(" / delta: ");
  Serial.print(qtouch.sliderDelta());
    
  wheel.update();
  delay(50);
}
