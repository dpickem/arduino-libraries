/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

// Include Pixel dependencies.
#include "enum.h"

String get_device_mode_type_string(int enumVal) {
  return DeviceModeTypeStrings[enumVal];
}

String get_device_group_type_string(int enumVal) {
  return DeviceGroupTypeStrings[enumVal];
}
