/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _JSON_UTILITIES_
#define _JSON_UTILITIES_

// Include Arduino library.
#include <Arduino.h>

// Include ArduinoJson library.
#include <ArduinoJson.h>

// Include Pixel dependencies.
#include "config.h"
#include "device_group_controller.h"

// Collection of JSON utility functions.
String get_config_json_string(DeviceGroupController* ctrl);

#endif //_JSON_UTILITIES_
