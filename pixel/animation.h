/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _ANIMATION_
#define _ANIMATION_

// Include Arduino libraries.
#include "Arduino.h"

// Include Pixel dependencies.
#include "animation_utilities.h"
#include "colors.h"

/*! @brief An abstract base class for animations.
 *
 */
class Animation {
  public:
    virtual void reset();
    virtual void start();
    virtual void update();
    virtual bool is_finished();
    virtual Color get_current_color();
    virtual String get_name();
};

class AnimationBlink : public Animation {
  public:
    // Constructor.
    AnimationBlink(Color color_on,
                   Color color_off,
                   uint32_t on_time_ms,
                   uint32_t off_time_ms,
                   uint8_t brightness = 255,
                   uint32_t repetitions = 0);

    // Destructor.
    ~AnimationBlink() {};

    // Core functions according to abstract base class.
    void reset();
    void start();
    void update();
    bool is_finished();
    Color get_current_color();
    String get_name();

  private:
    // Current state of animation.
    bool on_;
    Color color_current_;
    uint32_t repetition_count_;

    // Timing-related variables.
    uint32_t last_transition_time_ms_; 

    // Buffer parameters.
    Color color_on_;
    Color color_off_;
    uint32_t on_time_ms_;
    uint32_t off_time_ms_;
    uint8_t brightness_;
    uint32_t repetitions_;
};

class AnimationDimming : public Animation {

};

class AnimationColorWheel : public Animation {

};

#endif //_ANIMATION_
