/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _DEVICE_RANGE_
#define _DEVICE_RANGE_

// Include Arduino libraries.
#include "Arduino.h"

/*! @brief A templated class representing a numeric range.
 *
 *  NOTE: This class is NOT templated for the main reason that instances of
 *  templated classes with different template parameters cannot be stored
 *  easiliy in a single container in C++. Therefore, the most general numeric
 *  datatype float is used to represent all numeric device ranges.
 */
class DeviceRange {
  public:
    // Constructor.
    DeviceRange(float min_val, float max_val, float step);

    // Destructor.
    ~DeviceRange() {};

    // Utility functions.
    bool is_at_max();
    bool is_at_min();

    // Getter functions.
    float get_current();
    float get_next();
    float get_previous();
    float get_min();
    float get_max();
    float get_step();

    // Setter functions.
    void set_min(float min_val);
    void set_max(float max_val);
    void set_step(float step);
    void set_to_min();
    void set_to_max();

  private:
    // Class variables.
    float min_;
    float max_;
    float step_;
    float current_;
};

#endif //_DEVICE_RANGE_
