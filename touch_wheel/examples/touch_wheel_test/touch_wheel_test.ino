/***************************************************************************
  This is a library for the AT42QT2120 capacitive touch sensor.

  This sensor uses I2C to communicate, at a minimum 2 pins are required
  to interface.

  Written by Daniel Pickem.

  MIT license, all text above must be included in any redistribution
 ***************************************************************************/

#include "at42qt2120_qtouch.h"
#include "touch_wheel.h"

// Instantiate QTouch sensor.
// NOTE: On the Pixel v2.0, the touch wheel change pin is pin 12.
uint8_t changePin = 12;
AT42QT2120_QTouch qtouch(changePin);
TouchWheel wheel(&qtouch);

void setup() {
  // Open a serial connection for debugging.
  Serial.begin(115200);

  // Initialize touch wheel.
  wheel.begin();
}

void loop() {
  wheel.update();
  delay(50);
}
