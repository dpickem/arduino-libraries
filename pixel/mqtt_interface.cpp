/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#include "mqtt_interface.h"

// Initialize all static member variables.
CallbackHandler MQTTInterface::cb_handler_ = CallbackHandler();
WiFiClient MQTTInterface::wifi_client_ = WiFiClient();
MQTTClient MQTTInterface::mqtt_client_ = MQTTClient();


bool MQTTInterface::setup(bool connect_client) {
  // Setup WIFI.
  LOG_VALUE("Setting up WIFI client for SSID: ", WIFI_SSID);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);

  // Setup MQTT client.
  LOG_VALUE("Setting up MQTT client for broker IP: ", MQTT_BROKER_IP);
  MQTTInterface::mqtt_client_.begin(MQTT_BROKER_IP, MQTTInterface::wifi_client_);

  // Setup message callback function.
  // NOTE: A non-static member function cannot be used for onMessage
  //       registration because it requires an implicit 'this' to be passed
  //       into the function. Instead a static member function is passed in,
  //       which has access to static members of the class.
  // NOTE: A static member of a class means that no matter how many objects of
  //       the class are created, there is only one copy of the static member.
  //       (see https://www.tutorialspoint.com/cplusplus/cpp_static_members.htm)
  MQTTInterface::mqtt_client_.onMessage(MQTTInterface::process_incoming_message);

  // Connect WIFI and MQTT.
  if (connect_client) {
    LOG("Connecting to WIFI and MQTT broker...")
    MQTTInterface::connect();
  }
}

bool MQTTInterface::connect() {
  LOGNN("Connecting to WIFI...");
  while (WiFi.status() != WL_CONNECTED) {
    LOGNN(".");
    delay(1000);
  }
  LOG_VALUE("\nSuccessfully connected to SSID", WIFI_SSID);

  LOGNN("\nConnecting to MQTT broker...");
  while (!MQTTInterface::mqtt_client_.connect(DEVICE_NAME, MQTT_USERNAME, MQTT_PASSWORD)) {
    LOGNN(".");
    delay(1000);
  }
  LOG_VALUE("\nSuccessfully connected to broker IP", MQTT_BROKER_IP);

  LOG("\nSuccessfully connected to WIFI and MQTT broker.");
}

void MQTTInterface::update() {
  // Update the MQTT client.
  // NOTE: The client's loop function also triggers the previously registered
  //       process_incoming_message function, which in turn calls the appropriate
  //       callback for each topic-message pair received.
  MQTTInterface::mqtt_client_.loop();

  // The 10ms delay fixes some issues with WiFi stability.
  delay(10);  

  // Check if the client is connected and if not, reconnect.
  if (!MQTTInterface::mqtt_client_.connected()) {
    LOG("WiFi and MQTT client not connected. Trying to reconnect...");
    MQTTInterface::connect();
  }
}

bool MQTTInterface::register_topic_callback(String topic, Callback cb) {
  // Add callback handler for given topic.
  // NOTE: This method returns false if a callback for the
  //       given topic is already registered.
  if (MQTTInterface::cb_handler_.add_named_callback(topic, cb)) {
    // Subscribe to topic.
    MQTTInterface::mqtt_client_.subscribe(topic);

    // Report successfully registered callback
    MQTTInterface::error_handler(ErrorCode::MQTT_CALLBACK_SUCCESSFULLY_REGISTERED, String(""));
  } else {
    // Error handling: Callback for topic already registered.
    String error_msg = "Callback for topic " + topic + " already registered.";
    MQTTInterface::error_handler(ErrorCode::ERROR_MQTT_CALLBACK_ALREADY_REGISTERED, error_msg);
  }
}

bool MQTTInterface::unregister_topic(String topic) {
  // Find index of callback.
  int cb_idx = MQTTInterface::cb_handler_.get_callback_index_by_name(topic);

  // Try to remove callback if it was found.
  if (cb_idx >= 0) {
    // Unsubscribe from topic.
    MQTTInterface::mqtt_client_.unsubscribe(topic);

    // Delete callback from callback handler.
    return MQTTInterface::cb_handler_.remove_callback(topic);
  }

  return false;
}

void MQTTInterface::error_handler(ErrorCode error_code, String error_msg_opt) {
  // Compose JSON error string.
  String error_string = "{\"error_code\": " +
                        String(static_cast<int>(error_code)) +
                        ", " + "\"error_description\": " +
                        get_error_code_string(error_code) + 
                        ", \"details\": " + error_msg_opt + "}";

  // Publish error message on error topic.
  MQTTInterface::mqtt_client_.publish(topics_publish::TOPIC_ERROR, error_string);
}

void MQTTInterface::process_incoming_message(String& topic, String& message) {
  LOG_VALUE("Callback registered for topic " + topic, MQTTInterface::cb_handler_.is_callback_registered(topic));

  // TODO: Remove debug prints.
  Serial.println(topic);
  Serial.println(message);
  Serial.println();

  // Retrieve callback and execute it.
  Callback cb = MQTTInterface::cb_handler_.get_callback_by_name(topic);
  if (cb != nullptr) {
    // Check if message length exceeds max JSON buffer size.
    if (message.length() < JSON_BUFFER_SIZE_MAX) {
      // Parse message string into a parsed JSON object.
      ParsedJsonObject json_object(message);

      // Call callback and pass in JSON object.
      (*cb)(json_object);
    } else {
      MQTTInterface::error_handler(ErrorCode::ERROR_INPUT_STRING_EXCEEDS_MAX_LENGTH, String(""));
    }
  } else {
    // Error report missing callback.
    String error_msg = "Callback for topic " + topic + " not found.";
    MQTTInterface::error_handler(ErrorCode::ERROR_MQTT_CALLBACK_NOT_FOUND, error_msg);
  }
};

void MQTTInterface::test_callback(ParsedJsonObject& obj) {
    Serial.println("Test print of test callback function");

    // Test nested JSON access with CORRECT data type.
    const int time = obj.root()["time"];
    if (time) {
      LOG_VALUE("CALLBACK - Nested JSON access [time]: ", time);
      MQTTInterface::mqtt_client_.publish("/test_topic", "test_callback successfully parsed time");
    } else {
      LOG("CALLBACK - Nested JSON failed to access key 'temp'.");
    }
}
