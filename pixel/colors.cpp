/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

// Include Pixel dependencies.
#include "colors.h"
#include "utilities.h"

// Default constructor.
Color::Color() {
  // Return a random color
  color_.r = (uint8_t)random(255);
  color_.g = (uint8_t)random(255);
  color_.b = (uint8_t)random(255);
  color_.name = "random";
}

Color::Color(uint8_t r, uint8_t g, uint8_t b) {
  // Set color from raw components.
  color_.r = r;
  color_.g = g;
  color_.b = b;
  color_.name = "";
}

Color::Color(String color_name) {
  for(const ColorDescriptor &color: colors) {
    if (color.name == color_name) {
      color_.r = color.r;
      color_.g = color.g;
      color_.b = color.b;
      color_.name = color.name;
      return;
    }
  }

  // If the specified color name was not found, initialize white color.
  color_.r = 255;
  color_.g = 255;
  color_.b = 255;
  color_.name = "white";

  LOG_VALUE("Failed to find color with name", color_name);
}

void Color::print() {
  Serial.println("=== Color ===");
  Serial.print("  Name: ");
  Serial.println(color_.name);
  Serial.print("  R / G / B: ");
  Serial.print(color_.r);
  Serial.print(" / ");
  Serial.print(color_.g);
  Serial.print(" / ");
  Serial.println(color_.b);
}

ColorDescriptor Color::get_color_descriptor() {
  return color_;
}

String Color::get_color_name() {
  return color_.name;
}

void Color::set_rgb(uint8_t r, uint8_t g, uint8_t b) {
  color_.r = r;
  color_.g = g;
  color_.b = b;
  color_.name = "";
}
