/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

// Include Pixel dependencies.
#include "device_group_controller.h"

DeviceGroupController::~DeviceGroupController() {
  // TODO: The destructor has to clean up all device group
  // pointers in the linked list.
}

// Utility functions.
bool DeviceGroupController::add_device_group(DeviceGroup* group) {
  // Check if device group is already registered.
  if (get_device_group_by_name(group->get_name()) == nullptr) {
    // Add device mode to mode map.
    groups_.add(group);

    // Set current_group_ if this is the first mode added.
    if (groups_.size() == 1) {
      current_group_ = 0;
      LOG_VALUE("Setting current group to", groups_.get(current_group_)->get_name());
    }

    LOG_VALUE("Successfully added device group", group->get_name());
    return true;
  }

  LOG_VALUE("Failed to add already existing device group", group->get_name());
  return false;
}

/*! @brief Remove a device group from the map of available device groups.
 *  
 *  The behavior of this function is as follows:
 *
 *  1. If the device group with the specified name exists, it is removed and
 *     the current group is set to the first available group.
 *  2. If the device group does not exist, this function returns false.
 */
bool DeviceGroupController::remove_device_group(String name) {
  // TODO: Implement this function.
  /*
  if (groups_.size() > 0) {
    for(std::vector<DeviceGroup*>::iterator it = groups_.begin(); it != groups_.end(); ++it) {
      // Retrieve current device group pointer.
      DeviceGroup* group = *it;

      // If a group of requested name is found, delete it.
      if (group->get_name() == name) {
        // Delete specified mode.
        groups_.erase(it);

        // Set current mode to first available mode.
        current_group_ = 0;

        LOG_VALUE("Successfully deleted group", name);
        return true;
      }
    }

    LOG_VALUE("Failed to delete non-existant group", name);
  } else {
    LOG_VALUE("No groups found. Could not delete group", name);
  }

  return false;
  */
}

// Getter functions.
DeviceGroup* DeviceGroupController::get_current_device_group() {
  if (groups_.size() > 0) {
    //LOG_VALUE("Found current device group", groups_.get(current_group_)->get_name());
    return groups_.get(current_group_);
  }

  LOG("Failed to find current device group. No device groups registered");
  return nullptr;
}

DeviceGroup* DeviceGroupController::get_device_group_by_name(String name) {
  if (groups_.size() > 0) {
    // Find group with specified name.
    for(int i = 0; i < groups_.size(); i++){
      if (groups_.get(i)->get_name() == name) {
        return groups_.get(i);
      }
    }

    LOG_VALUE("Failed to find device group", name);
  } else {
    LOG("Failed to find device group by name. No device groups registered");
  }

  return nullptr;
}

DeviceGroup* DeviceGroupController::get_device_group_by_index(uint8_t index) {
  if (groups_.size() > 0) {
    if (index >= 0 && index < groups_.size()) {
      return groups_.get(index);
    }
    LOG_VALUE("Failed to find device group because index is invalid", index);
  } else {
    LOG("Failed to find device group by index. No device groups registered");
  }

  return nullptr;
}

int DeviceGroupController::get_number_of_device_groups() {
  return groups_.size();
}

// Setter functions.
bool DeviceGroupController::set_next_device_group() {
  if (groups_.size() > 0) {
    //LOG_VALUE("Current group before", current_group_);
    current_group_++;

    if (current_group_ >= groups_.size()) {
      current_group_ = 0;
      LOG("Setting next device group rollover from last to first.");
    }

    //LOG_VALUE("Current group after ", current_group_);
    //LOG_VALUE("Current group count", groups_.size());
    //LOG_VALUE("Setting next device group to", groups_[current_group_]->get_name());
    return true;
  } else {
    LOG("Failed to set next device group. No device groups registered");
  }

  return false;
}

bool DeviceGroupController::set_previous_device_group() {
  if (groups_.size() > 0) {
    //LOG_VALUE("Current group", current_group_);
    current_group_--;

    if (current_group_ < 0) {
      current_group_ = groups_.size() - 1;
      LOG("Setting previous device group rollover from first to last.");
    }

    LOG_VALUE("Current group", current_group_);
    LOG_VALUE("Current group count", groups_.size());
    LOG_VALUE("Setting previous device group to", groups_.get(current_group_)->get_name());
    return true;
  } else {
    LOG("Failed to set previous device group. No device groups registered");
  }

  return false;
}
