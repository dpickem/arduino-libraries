/***************************************************************************
  This is a library for the AT42QT2120 capacitive touch sensor.

  This sensor uses I2C to communicate, at a minimum 2 pins are required
  to interface.

  Written by Daniel Pickem.

  MIT license, all text above must be included in any redistribution
 ***************************************************************************/

// Include touch wheel and capacitive touch driver.
#include "at42qt2120_qtouch.h"
#include "touch_wheel.h"

// Include Wifi and MQTT support.
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Include JSON library.
#include <ArduinoJson.h>

// Set up network parameters.
const char* ssid = "Y&D";
const char* password = "YT19DPcherry!!!";
const char* mqtt_server = "192.168.0.116";
String clientId = "ESP8266Client-30AC";
const char* topic = "/touch_wheel/pos";

// Instantiate QTouch sensor.
// NOTE: On the NodeMCU board, use pin D7 (i.e. pin 13).
//       On a regular Arduino Nano, use pin 3.
uint8_t changePin = 13;
AT42QT2120_QTouch qtouch(changePin);
TouchWheel wheel(&qtouch);

// Initialize Wifi client.
WiFiClient espClient;
PubSubClient client(espClient);

// Initialize message buffer.
char mqtt_data[100];

/*
StaticJsonBuffer<200> jsonBuffer;
JsonObject& root = jsonBuffer.createObject();

// Add data to JSON structure.
root["pos"] = "gps";
root["delta"] = 1351824120;

// Copy data to char buffer.
root.printTo((char*)jsonChar, root.measureLength() + 1);
*/

void setup_wifi() {
  delay(50);
  Serial.print("Connecting to ");
  Serial.println(ssid);

  // Connect to Wifi.
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.print("WiFi connected, IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");

    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("MQTT server connected.");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
    } else {
      Serial.print("Failed to reconnect rc=");
      Serial.print(client.state());
      Serial.println(". trying again in 5 seconds...");

      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  // Open a serial connection for debugging.
  Serial.begin(115200);

  // Initialize touch wheel.
  wheel.begin();

  // Initialize Wifi.
  setup_wifi();

  // Connect to MQTT server.
  client.setServer(mqtt_server, 1883);
}

void loop() {
  // Reconnect MQTT client if necessary.
  if (!client.connected()) {
    reconnect();
  }

  // Process callbacks for MQTT.
  client.loop();

  // Update touch wheel.
  wheel.update();

  // Create message.
  snprintf (mqtt_data, 100, "{\"pos\": %ld, \"delta\": %ld}", wheel.getSliderPosition(), wheel.getSliderDelta());

  // Publish via MQTT.
  client.publish(topic, mqtt_data);

  delay(50);
}
