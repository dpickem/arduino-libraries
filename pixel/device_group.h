/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _DEVICE_GROUP_
#define _DEVICE_GROUP_

// Include Arduino libraries.
#include "Arduino.h"

// Include STL containers.
#include "LinkedList.h"

// Include Pixel dependencies.
#include "device_mode.h"
#include "utilities.h"

/*! @brief A class representing a device group.
 *  
 *  A device group contains one or more devices that are controlled as a unit.
 */
class DeviceGroup {
  public:
    // Constructor
    DeviceGroup(String name, String color_name) : modes_() {
      // Set device group and color name.
      name_  = name;
      color_ = color_name;

      // Initialize map iterator.
      current_mode_ = 0;
    };

    // Destructor
    ~DeviceGroup();

    // Device mode management.
    bool add_device_mode(DeviceMode* mode);
    bool remove_device_mode(String name);

    bool set_next_device_mode();
    bool set_previous_device_mode();

    bool set_current_device_mode_to_max();
    bool set_current_device_mode_to_min();

    bool update_current_device_mode(float min_val,
                                    float max_val,
                                    float step);
    void print_device_group_details();

    // Getter functions.
    String get_name();
    int get_number_of_device_modes();
    String get_color();

    DeviceMode* get_current_device_mode();
    DeviceMode* get_device_mode_by_name(String name);
    DeviceMode* get_device_mode_by_index(uint8_t index);

    // Setter functions.
    void set_name(String name);
    void set_color(String color_name);

  private:
    // Class variables.
    String name_;

    // Index pointing to current device mode.
    int8_t current_mode_;

    // List of device modes.
    LinkedList<DeviceMode*> modes_;

    // Color associated with device group.
    String color_;
};

#endif //_DEVICE_GROUP_
