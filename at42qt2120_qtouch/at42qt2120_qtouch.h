/*
 * This is a library for the Atmel QTouch AT42QT2120
 * But it should work with other QTouch Devices as well
 *
 * This software is part of the firmware running Little Helper. You may use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software 
 * but you must include this copyright notice and this permission in all copies or 
 * substantial portions of the Software.
 *
 * Appfruits invests time and resources to make this open source. Please support us
 * and the open-source hardware initiative.
 * 
 * Copyright 2014-2015 Phillip Schuster (@appfruits)
 * http://www.appfruits.com
 * 
 * MIT-License
 *
 * Modified 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 */

#define AT42QT2120_DEBUG

#ifndef _AT42QT2120_
#define _AT42QT2120_

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif
#include <Wire.h>

#define SDA_PIN 2
#define SCL_PIN 14
#define QT2120_I2C_ADDRESS 0x1C
#define QT2120_DETECTION_STATUS 2
#define QT2120_KEYSTATUS_1 3 
#define QT2120_KEYSTATUS_2 4 
#define QT2120_SLIDERPOS 5
#define QT2120_SLIDER_OPTIONS 14
#define QT2120_DETECT_THRESHOLD 16
#define QT2120_DETECTION_INTEGRATOR 11
#define QT2120_NUMBER_OF_SAMPLES 1

/*! @brief An abstract class defining a delegate for the QTouch
 *         sensor class.
 *
 *  An implementation of this class can be used in higher level
 *  functions to interface with the low-level capacitive touch
 *  hardware.
 */
class AT42QT2120_QTouch_Delegate {
  public:
    /* Button callback functions for the four quadrants of the
     * touch sensor wheel.
     */
    virtual void onTouchLeftButtonPressed(uint32_t duration) {};
    virtual void onTouchTopButtonPressed(uint32_t duration) {};
    virtual void onTouchRightButtonPressed(uint32_t duration) {};
    virtual void onTouchBottomButtonPressed(uint32_t duration) {};

    // Slider position and delta callback.
    virtual void onSlider(uint8_t sliderPos, int8_t sliderDelta) {};

    // Button touch down and up callbacks.
    virtual void onTouchDown() {};
    virtual void onTouchUp(uint32_t duration) {};
};

/*! @brief The driver for the AT42QT2120 QTouch sensor.
 *
 *  The low-level driver class that interfaces with the QTouch
 *  sensor through I2C and an interrupt pin (changePin).
 */
class AT42QT2120_QTouch {
  public:
    // Constructor.
    AT42QT2120_QTouch(uint8_t changePin);

    // Function that sets up interrupt pin and I2C.
    bool begin(uint8_t sda_pin = SDA_PIN, uint8_t scl_pin = SCL_PIN);

    // Mode-changing functions that enable slider/wheel mode.
    void enableSlider(void);
    void enableWheel(void);

    // Mode detection functions.
    bool touchDetected();
    bool sliderDetected();
    bool wheelDetected();

    // Interrupt-triggered event detection.
    bool eventAvailable();

    // Event detection functions.
    bool isKeyPressed(uint8_t index);
    bool isDragging();

    // Functions for reading position and change in position
    // in slider/wheel mode.
    uint8_t sliderPosition();
    int8_t sliderDelta();
    uint8_t wheelPosition();
    int8_t wheelDelta();
    uint8_t getLastSliderPos();

    // Enable/disable dragging mode.
    void setDragging(bool dragging);

    // Set and get parameter functions.
    void setDetectThreshold(uint8_t index, uint8_t value);
    void setDetectionIntegrator(uint8_t value);
    uint8_t getDetectThreshold(uint8_t index);
    uint8_t getDetectionIntegrator();

    // Set the pointer to the delegate class.
    void setDelegate(AT42QT2120_QTouch_Delegate* delegate);

  private:
    // Class variables.
    AT42QT2120_QTouch_Delegate* _delegate;
    uint8_t   _lastSliderPos;
    bool      _dragging;
    uint32_t  _dragStartTime;
    uint8_t   _maxDelta;
    uint8_t   _changePin;
    float     _deltas[QT2120_NUMBER_OF_SAMPLES];
    uint8_t   _currentDelta;

    // Private class functions.
    bool isBitSet(uint8_t b, uint8_t n);
    uint8_t readRegister8(uint8_t reg);
    uint16_t readRegister16(uint8_t reg);
    void writeRegister(uint8_t reg, uint8_t value);
    void debugPrint(String function_name, uint32_t value);
};

#endif
