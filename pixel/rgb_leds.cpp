/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

// Include utilities.
#include "rgb_leds.h"

RGBLeds::RGBLeds(uint8_t number_of_leds,
                 uint8_t led_pin,
                 uint8_t brightness) {
  // Buffer input arguments.
  number_of_leds_ = number_of_leds;
  led_pin_ = led_pin;
  brightness_max_ = brightness;

  // Initialize NeoPixels.
  leds_ = Adafruit_NeoPixel(number_of_leds_,
                            led_pin_,
                            NEO_GRB + NEO_KHZ800);

  // Set Pixel brightness to default value.
  // NOTE: setBrightness is meant to be called once in the setup function,
  //       not repeatedly when setting pixel values.
  leds_.setBrightness(brightness_max_);
}

void RGBLeds::begin() {
  LOG("Initializing pixels");
  leds_.begin();
  leds_.setBrightness(brightness_max_);
}

void RGBLeds::initialize() {
  // Initialize LEDs with color white and brightness 0.
  for (uint8_t i = 0; i < number_of_leds_; i++) {
    set_led_color(255, 255, 255, i, 0);
  }
}

uint8_t RGBLeds::get_number_of_leds() {
  return number_of_leds_;
}

uint8_t RGBLeds::get_led_pin() {
  return led_pin_;
}

bool RGBLeds::set_led_color(ColorDescriptor color,
                            int8_t led_index,
                            uint8_t brightness) {
  LOG_VALUE("Setting LED color of index", led_index);
  return set_led_color(color.r, color.g, color.b, led_index, brightness);
}

bool RGBLeds::set_led_color(uint8_t r,
                            uint8_t g,
                            uint8_t b,
                            int8_t led_index,
                            uint8_t brightness) {
  if (led_index < 0) {
    // If a negative index is provided (default value), set all leds
    // in the strip to the provided color.
    /*
    for (uint8_t i = 0; i < number_of_leds_; i++) {
      leds_.setPixelColor(i, r, g, b);
    }
    */
    return false;
  } else if (led_index > number_of_leds_) {
    // If an index larger than the led count is provided, don't set
    // any leds and return false.
    return false;
  } else {
    // Compute brightness scale for RGB values with a max value of 1.0.
    float scale = min(static_cast<float>(brightness) / static_cast<float>(brightness_max_), 1.0);

    Serial.print("RGB LED scale value: ");
    Serial.println(scale);

    Serial.print("Scaled R value: ");
    Serial.println(r * scale);

    // Default case is where a valid positive index is provided.
    leds_.setPixelColor(led_index,
                        static_cast<uint8_t>(static_cast<float>(r) * scale),
                        static_cast<uint8_t>(static_cast<float>(g) * scale),
                        static_cast<uint8_t>(static_cast<float>(b) * scale));
  }

  // Write data to NeoPixels.
  leds_.show();

  return true;
}
