/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

// Include Pixel dependencies.
#include "json_utilities.h"

String get_config_json_string(DeviceGroupController* ctrl) {
  // Initialize configuration JSON object.
  DynamicJsonBuffer jsonBuffer(1024);
  JsonObject& root = jsonBuffer.createObject();

  // Add device identifying information.
  JsonObject& identifier = root.createNestedObject("identifier");
  identifier["device_name"] = DEVICE_NAME;
  identifier["device_uuid"] = DEVICE_UUID;

  // Add all available device groups.
  JsonArray& groups = root.createNestedArray("device_groups");

  for (uint8_t i = 0; i < ctrl->get_number_of_device_groups(); i++) {
    // Get device group by index.
    const auto device_group = ctrl->get_device_group_by_index(i);

    // For each device group add all mode.
    JsonObject& group = groups.createNestedObject();
    group["name"] = device_group->get_name();

    // For each mode add range parameters.
    JsonArray& modes = group.createNestedArray("device_modes");

    for (uint8_t j = 0; j < device_group->get_number_of_device_modes(); j++) {
      // Get device mode by index.
      const auto device_mode = device_group->get_device_mode_by_index(j);
      const auto device_range = device_mode->get_device_range();

      // For each device mode add all parameters.
      JsonObject& mode = modes.createNestedObject();
      mode["name"] = device_mode->get_name();
      mode["range_min"] = device_range->get_min();
      mode["range_max"] = device_range->get_max();
      mode["range_step"] = device_range->get_step();
      mode["range_current"] = device_range->get_current();
    }
  }

  // Add supported commands.
  // TODO: Make this more general than adding individual elements.
  JsonArray& cmds = root.createNestedArray("supported_commands");
  cmds.add(topics_subscribe::TOPIC_GET_STATE);

  // Print JSON object.
  root.prettyPrintTo(Serial);

  // Return JSON string.
  String config_json_string;
  root.printTo(config_json_string);
  return config_json_string;
}
