/*
 * This is a library for the Atmel QTouch AT42QT2120
 * But it should work with other QTouch Devices as well
 *
 * This software is part of the firmware running Little Helper. You may use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software 
 * but you must include this copyright notice and this permission in all copies or 
 * substantial portions of the Software.
 *
 * Appfruits invests time and resources to make this open source. Please support us
 * and the open-source hardware initiative.
 * 
 * Copyright 2014-2015 Phillip Schuster (@appfruits)
 * http://www.appfruits.com
 * 
 * MIT-License
 *
 * Modified 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 */

// Include QTouch header file.
#include "at42qt2120_qtouch.h"

/**********************************************************
 *                  PUBLIC FUNCTIONS
 **********************************************************/

/*! @brief Class constructor for QTouch sensor driver
 *
 * @param changePin Interrupt pin to be checked for changes
 *                  in state.
 */
AT42QT2120_QTouch::AT42QT2120_QTouch(uint8_t changePin) {
  // Initialize delegate pointer.
	_delegate = NULL;

  // Buffer interrupt pin number.
	_changePin = changePin;

  // Initialize sampled deltas and current delta.
	for (int i=0; i<QT2120_NUMBER_OF_SAMPLES; i++){
		_deltas[i] = 0.0f;
	}
	
	_currentDelta = 0;
}

/*! @brief Initialize the QTouch instance.
 *
 *  In particular this function sets the pin mode to input
 *  for the interrupt pin and initializes I2C. A basic
 *  function check is performed on the AT42QT2120 chip to
 *  ensure the chip is in operational condition.
 *
 * @return True if the initialization has been successful,
 *         false otherwise.
 */
bool AT42QT2120_QTouch::begin(uint8_t sda_pin, uint8_t scl_pin) {
  // Set interrupt pin mode to input.
	pinMode(_changePin, INPUT);

  // Start I2C communication.
	Wire.begin(sda_pin, scl_pin);

  /* Perform error checking on whether the sensor was found. The
   * following check uses code similar to the I2C scanner example.
   *
   * See https://www.arduino.cc/en/Reference/WireEndTransmission
   *
   * Possible return codes of Wire.endTransmission() are the
   * following:
   *
   * 0: success
   * 1: data too long to fit in transmit buffer
   * 2: received NACK on transmit of address
   * 3: received NACK on transmit of data
   * 4: other error
   */

  // Initiate transmission to AT42QT2120 sensor.
  Wire.beginTransmission(QT2120_I2C_ADDRESS);
  byte error = Wire.endTransmission();

  if (error == 0) {
    Serial.print("AT42QT2120 device found at address ");
    Serial.print(QT2120_I2C_ADDRESS, HEX);
    return true;
  } else {
    Serial.print("Failed to find AT42QT2120 at address 0x");
    Serial.print(QT2120_I2C_ADDRESS, HEX);
    Serial.print(" with error code ");
    Serial.print(error);
    Serial.println(".");

    return false;
  }
}

/*! @brief Enable sliding mode for the QTouch instance */
void AT42QT2120_QTouch::enableSlider() {
	writeRegister(QT2120_SLIDER_OPTIONS, 0x80);  //Enable Slider Bit
}

/*! @brief Enable wheel mode for the QTouch instance */
void AT42QT2120_QTouch::enableWheel() {
	writeRegister(QT2120_SLIDER_OPTIONS, 0xC0);  //Enable WHEEL Bit
}

/*! @brief Determine whether the QTouch instance has
 *         detected a touch.
 *
 * @return True if the QTouch instance detected a touch,
 *         false otherwise.
 */
bool AT42QT2120_QTouch::touchDetected() {
	uint8_t detectionStatus = readRegister8(QT2120_DETECTION_STATUS);
	if (isBitSet(detectionStatus,0)) {
		return true;
	}

	return false;
}

/*! @brief Determine whether the QTouch instance is in
 *         slider/wheel mode.
 *
 * @return True if the QTouch instance is in slider/wheel
 *         mode, false otherwise.
 */
bool AT42QT2120_QTouch::sliderDetected() {
	uint8_t detectionStatus = readRegister8(QT2120_DETECTION_STATUS);
	if (isBitSet(detectionStatus,1)) {
		return true;
	}

	this->setDragging(false);

	return false;
}

/*! @see AT42QT2120_QTOUCH::sliderDetected */
bool AT42QT2120_QTouch::wheelDetected() {
	return sliderDetected();
}

/*! @brief Determine whether an event is available.
 *
 *  This function uses the interrupt / change pin set when
 *  initializing the QTouch instance
 *
 * @return True if an event is available (i.e. the change
 *         pin has been pulled to low), false otherwise.
 */
bool AT42QT2120_QTouch::eventAvailable() {
	if (digitalRead(_changePin) == LOW) {
		return true;
	}

	this->setDragging(false);
	_lastSliderPos = 0;

	return false;
}

/*! @brief Determine whether a specified key is pressed.
 *
 * @param index The index of the key to be checked
 *        (range 0..11).
 *
 * @return True if the button with the specified index has
 *         been pressed, false otherwise.
 */
bool AT42QT2120_QTouch::isKeyPressed(uint8_t index) {
	if (index > 11) return false;

	uint8_t address = QT2120_KEYSTATUS_1;
	if (index > 7) {
		address = QT2120_KEYSTATUS_2;
		index -= 8;
	}

	uint8_t keyStatus = readRegister8(address);
	return isBitSet(keyStatus,index);
}

/*! @brief Check whether the QTouch instance is in dragging
 *         mode.
 *
 * /TODO: Explain what dragging mode is.
 *
 * @returns True if dragging mode is active, false otherwise.
 */
bool AT42QT2120_QTouch::isDragging() {
	return this->_dragging;
}

/*! @brief Return the absolute slider position.
 *
 * @return The absolute position value of the slider (range 0..255).
 */
uint8_t AT42QT2120_QTouch::sliderPosition() {
	uint8_t sliderPos = readRegister8(QT2120_SLIDERPOS);
	return sliderPos;
}

/*! @brief Compute the delta in slider value.
 *
 * The logic flow of this function is as follows.
 * 
 \verbatim
 If "sliding mode" detected:
    If "dragging mode" is active:
        Update delegate.
    Else:
        Activate "dragging mode".
 Else:
    Deactivate dragging mode.
 \endverbatim
 *
 * @return The change in slider position.
 */
int8_t AT42QT2120_QTouch::sliderDelta() {
	if (sliderDetected()) {
		uint8_t sliderPos = sliderPosition();		

		if (this->_dragging == true) {
			int16_t delta = sliderPos - _lastSliderPos;

      // Ensure the slider delta remains in range -128..128.
      if (delta < -128) { delta += 255; }
      if (delta > 128) { delta -= 255; }

      // Update the current delta buffer.
      _deltas[_currentDelta++] = delta;
      if (_currentDelta > (QT2120_NUMBER_OF_SAMPLES-1)) {
        _currentDelta = 0;
      }

      // Update the slider delta value.
      delta = 0;
      for (int i=0; i<QT2120_NUMBER_OF_SAMPLES; i++) {
        delta += _deltas[i];
      }
      delta /= QT2120_NUMBER_OF_SAMPLES;
        
      // Buffer current slider position.
      _lastSliderPos = sliderPos;

      // Compute maximum slider delta seen since last reset
      // in the setDragging function.
			if (abs(delta) > this->_maxDelta) {
				this->_maxDelta = delta;
			}

      // If a delegate has been registered, update its
      // callback functions.
			if (this->_delegate != NULL) {
				if (abs(this->_maxDelta) > 1) {
          // NOTE: maxDelta > 1 means that setDragging has
          //       been set to true by a previous interaction
          //       and we are now in sliding mode.
					this->_delegate->onSlider(sliderPos, delta);
				} else {
          // NOTE: maxDelta < 1 means the touch interaction
          //       has just been initiated and sliding mode
          //       was not active previously.
					this->_delegate->onTouchDown();
				}
			}

      return delta;
		} else {
      // If dragging mode has not been active previously,
      // activate it now and buffer the current slider
      // position.
      _lastSliderPos = sliderPos;
      this->setDragging(true);
    }
	} else {
    // NOTE: Dragging mode can only be active in sliding mode.
		this->setDragging(false);
	}

	return 0;
}

/*! @see AT42QT2120_QTOUCH::sliderPosition */
uint8_t AT42QT2120_QTouch::wheelPosition() {
	return sliderPosition();
}

/*! @see AT42QT2120_QTOUCH::sliderDelta */
int8_t AT42QT2120_QTouch::wheelDelta() {
	return sliderDelta();
}

/*! @brief Get the last known slider position of the QTouch
 *         instance.
 *
 * @returns The last known slider position.
 */
uint8_t AT42QT2120_QTouch::getLastSliderPos() {
	return this->_lastSliderPos;
}

/*! @brief Set the dragging mode.
 *
 * The logic flow of this function is as follows:
 * 
 \verbatim
 If "dragging mode" is unchanged:
    return
 Else if "dragging mode" changed to ON:
    Call the onTouchDown callback.
 Else if "dragging mode" changed to OFF:
    Call the onTouchUp callback.

    If maxDelta is small:
        Call the respective button callbacks
    Else:
        Ignore, since the action was a sliding motion.
 \endverbatim
 *
 * @param dragging Value of dragging mode to be set.
 */
void AT42QT2120_QTouch::setDragging(bool dragging) {
  // Only execute this function if a change in dragging
  // mode is registered.
	if (_dragging == dragging) { return; }

	if (dragging) {
    // If we just ENTERED dragging mode record dragging
    // start time.
		this->_dragStartTime = millis();

    // Call onTouchDown function in delegate.
		if (this->_delegate != NULL) {
			this->_delegate->onTouchDown();
		} else {
      // Non-delegate mode for debugging.
      debugPrint("onTouchDown", this->_dragStartTime);
    }
	} else {
    // If we just EXITED dragging mode, compute the button
    // press / sliding duration.
		uint32_t duration = millis()-this->_dragStartTime;

    // Call the onTouchUp callback in the delegate.
		if (this->_delegate != NULL) {
			this->_delegate->onTouchUp(duration);
		} else {
      // Non-delegate mode for debugging.
      debugPrint("onTouchUp", duration);
    }

    // A small _maxDelta value indicates a button touch not
    // a sliding/dragging motion. Call the callback function
    // corresponding to the correct quadrant on the wheel.
    //
    // Total range: 0 .. 255
    //
    // 0   ... 63   LEFT
    // 64  ... 127  TOP
    // 128 ... 195  RIGHT
    // 196 ... 255  BOTTOM
    //
		if (this->_maxDelta <= 1) {
			if (_delegate != NULL) {
				//Just tapped and not dragged.
				if (_lastSliderPos >= 0 && _lastSliderPos < 64) {
					this->_delegate->onTouchLeftButtonPressed(duration);
				} else if (_lastSliderPos >= 64 && _lastSliderPos < 128) {
					this->_delegate->onTouchTopButtonPressed(duration);
				} else if (_lastSliderPos >= 128 && _lastSliderPos < 196) {
					this->_delegate->onTouchRightButtonPressed(duration);
				} else {
					this->_delegate->onTouchBottomButtonPressed(duration);
				}
        /*
				if (abs(_lastSliderPos-195) < 10) {
					this->_delegate->onTouchLeftButtonPressed(duration);
				} else if (abs(_lastSliderPos-127) < 10) {
					this->_delegate->onTouchTopButtonPressed(duration);
				} else if (abs(_lastSliderPos-35) < 10) {
					this->_delegate->onTouchRightButtonPressed(duration);
				} else if (abs(_lastSliderPos-90) < 10) {
					this->_delegate->onTouchBottomButtonPressed(duration);
				}
         */
			} else {
        // Non-delegate mode for debugging.
				if (abs(_lastSliderPos-140) < 10) {
          debugPrint("onTouchLeftButtonPressed", duration);
				} else if (abs(_lastSliderPos-220) < 10) {
          debugPrint("onTouchTopButtonPressed", duration);
				} else if (abs(_lastSliderPos-35) < 10) {
          debugPrint("onTouchRightButtonPressed", duration);
				} else if (abs(_lastSliderPos-90) < 10) {
          debugPrint("onTouchBottomButtonPressed", duration);
				}
      }
		}
	}

  // Upon termination of this function reset _maxDelta,
  // which is used in the sliderDelta function.
	this->_dragging = dragging;
	this->_maxDelta = 0;
}

/*! @brief Set the detection threshold value on the chip.
 *
 *  Details can be found below or in the datasheet of the
 *  AT42QT2120 chip at:
 *  http://ww1.microchip.com/downloads/en/DeviceDoc/doc9634.pdf
 *
 *  The device detects a touch when the signal has crossed a
 *  threshold level and remained there for a specified number
 *  of counts. This can be altered on a key-by-key basis
 *  using the key Detect Threshold I2C-compatible commands.
 *
 * @param index The key for which the detection threshold
 *              is to be set.
 * @param value The detector threshold to be set for the key
 *              given by index.
 */
void AT42QT2120_QTouch::setDetectThreshold(uint8_t index, uint8_t value) {
  // Check valid key index.
	if (index > 11) {
    return;
  }

  // Write threshold to key register.
	uint8_t address = QT2120_DETECT_THRESHOLD + index;
	writeRegister(address, value);
}


/*! @brief Retrieve the detection threshold value from the
 *         chip's register.
 *
 * @param index The index of the key for which the detection
 *              threshold is to be retrieved.
 * @return The currently set detection threshold value.
 */
uint8_t AT42QT2120_QTouch::getDetectThreshold(uint8_t index) {
  // Retrieve threshold from key register.
	uint8_t address = QT2120_DETECT_THRESHOLD + index;
	return readRegister8(address);
}

/*! @brief Set the detection integrator value on the chip.
 *
 *  Details can be found below or in the datasheet of the
 *  AT42QT2120 chip at:
 *  http://ww1.microchip.com/downloads/en/DeviceDoc/doc9634.pdf
 *
 *  The device features a fast detection integrator counter
 *  (DI filter), which acts to filter out noise at the small
 *  expense of a slower response time. The DI filter requires
 *  a programmable number of consecutive samples confirmed in
 *  detection before the key is declared to be touched. The
 *  minimum number for the DI filter is 1. A setting of 0 for
 *  the DI also defaults to 1. The DI has a maximum usable
 *  value of 32. Values above this will prevent a key from
 *  entering touch.
 *
 * @param The detection integrator value to be set.
 */
void AT42QT2120_QTouch::setDetectionIntegrator(uint8_t value) {
	if (value <= 0) value = 1;
	if (value > 32) value = 32;

	writeRegister(QT2120_DETECTION_INTEGRATOR,value);
}

/*! @brief Retrieve the detection integrator value from the
 *         chip's register.
 *
 * @return The currently set detection integrator value.
 */
uint8_t AT42QT2120_QTouch::getDetectionIntegrator() {
	return readRegister8(QT2120_DETECTION_INTEGRATOR);
}

/*! @brief Setter functions to add a delegate to the QTouch
 *         instance.
 *
 * @param delegate A pointer to the delegate instance.
 */
void AT42QT2120_QTouch::setDelegate(AT42QT2120_QTouch_Delegate *delegate) {
	this->_delegate = delegate;
}

/**********************************************************
 *                  PRIVATE FUNCTIONS
 **********************************************************/

/*! @brief Helper function to check whether a bit is set in
 *         an 8 bit input value.
 *
 * @param b The 8 bit data value.
 * @param n The n-th bit is checked whether it is set or not.
 * @return True if the n-th bit in b is set, false otherwise.
 */
bool AT42QT2120_QTouch::isBitSet(uint8_t b, uint8_t n) { 
  return b & ( 1 << n); 
}

/*! @brief Helper function to read 8 bits of data from a 
 *         register via I2C.
 *
 * @param reg The register address to read from.
 * @return Data as uint8_t value.
 */
uint8_t AT42QT2120_QTouch::readRegister8(uint8_t reg) {
    Wire.beginTransmission(QT2120_I2C_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission(false);
    Wire.requestFrom(QT2120_I2C_ADDRESS, 1);
    return ( Wire.read());
}

/*! @brief Helper function to read 16 bits of data from two
 *         registers via I2C.
 *
 * @param reg The register address to read from.
 * @return Data as uint8_16 value.
 */
uint16_t AT42QT2120_QTouch::readRegister16(uint8_t reg) {
    Wire.beginTransmission(QT2120_I2C_ADDRESS);
    Wire.write(reg);
    Wire.endTransmission(false);
    Wire.requestFrom(QT2120_I2C_ADDRESS, 2);
    uint16_t v = Wire.read();
    v |=  ((uint16_t) Wire.read()) << 8;
    return v;
}

/*! @brief Helper function to write data to a register via I2C.
 *
 * @param reg The register address to write to.
 * @param value The 8 bit value to write to the specified register.
 */
void AT42QT2120_QTouch::writeRegister(uint8_t reg, uint8_t value) {
    Wire.beginTransmission(QT2120_I2C_ADDRESS);
    Wire.write((uint8_t)reg);
    Wire.write((uint8_t)(value));
    Wire.endTransmission();
}

void AT42QT2120_QTouch::debugPrint(String function_name, uint32_t value) {
#ifdef AT42QT2120_DEBUG
  Serial.print(function_name);
  Serial.print(" called with value ");
  Serial.println(value);
#endif
}
