This is a library for the Pixel v2.0 Touch HMI.

The Pixel is a custom-designed interface for home automation and features
a touch interface with an IPod-like touch wheel.

Sources:
- https://github.com/limpkin/mooltipass/blob/master/arduino/standard/libraries/mooltipass_touch_sensing/mooltipass_touch_sensing.h
