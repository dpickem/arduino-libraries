/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _CALLBACK_HANDLER_
#define _CALLBACK_HANDLER_

// Include Arduino library.
#include <Arduino.h>

// Include STL containers.
#include "LinkedList.h"

// Include Pixel dependencies.
#include "json_parsing.h"
#include "utilities.h"

typedef void(*Callback)(ParsedJsonObject&);

struct NamedCallback {
  String name;
  Callback fcn;
};

class CallbackHandler {
  public:
    // Constructor.
    CallbackHandler() {};

    // Destructor.
    ~CallbackHandler() {};

    // Utility functions.
    bool is_callback_registered(String name);
    bool add_named_callback(String name, Callback fcn);
    bool remove_callback(String name);

    // Getter functions.
    Callback get_callback_by_name(String name);
    int get_callback_index_by_name(String name);

  private:
    // List of device modes.
    LinkedList<NamedCallback> callbacks_;
};

#endif //_CALLBACK_HANDLER_
