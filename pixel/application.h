/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _APPLICATION_
#define _APPLICATION_

// Include external sensor libraries.
#include <Adafruit_NeoPixel.h>

// Include configuration file.
#include "config.h"

// Include Pixel sensor libraries.
#include "at42qt2120_qtouch.h"
#include "button.h"
#include "touch_wheel.h"

// Include Pixel output libraries.
#include "rgb_leds.h"

// Include Pixel MQTT interface.
#include "mqtt_interface.h"

// Include Pixel dependencies.
#include "colors.h"
#include "device_range.h"
#include "device_mode.h"
#include "device_group.h"
#include "device_group_controller.h"
#include "json_utilities.h"
#include "utilities.h"

class Application {
  public:
    // Constructor.
    Application() : button_(BUTTON_PIN),
                    leds_(RGB_LEDS_NUM,
                          RGB_LEDS_PIN,
                          RGB_LEDS_BRIGHTNESS),
                    qtouch_(TOUCH_CHANGE_PIN),
                    touch_wheel_(&qtouch_) {}

    // Destructor.
    ~Application() {}

    // Initialize function.
    void setup();
    void setup_mqtt();
    void setup_device_groups();

    // Main loop.
    void loop();

    // Utility functions.
    void process_touch_wheel_actions();
    void process_button_actions();
    void update_leds();

    // MQTT callback functions.
    static void callback_request(ParsedJsonObject& obj);
    static void callback_get_state(ParsedJsonObject& obj);
    static void callback_get_data(ParsedJsonObject& obj);
    static void callback_get_config(ParsedJsonObject& obj);
    static void callback_set_config(ParsedJsonObject& obj);

  private:
    // Declare Pixel sensors.
    AT42QT2120_QTouch qtouch_;
    TouchWheel touch_wheel_;
    Button button_;

    // Declare Pixel outputs.
    RGBLeds leds_;

    // Declare device group controller.
    static DeviceGroupController device_group_controller_;

    // Declare MQTT interface.
    static MQTTInterface mqtt_interface_; 

    // Declare flags.
    bool update_group_led_;
    bool update_mode_led_;
};

#endif // _DEVICE_GROUP_
