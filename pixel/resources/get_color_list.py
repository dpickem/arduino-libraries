import re

def get_color_from_string(s):
  """
  Colors are of the form: 'lavender blush  #FFF0F5 (255,240,245)'
  """
  if '/' in s:
    return None, None
  else:
    # Extract color name first.
    s = s.split('#')
    color_name = s[0].rstrip().lstrip().lower()

    # Extract RGB value from remaining string.
    comp = s[-1].split(' ')

    # Get color RGB values.
    rgb = comp[-1].rstrip('\n')
    rgb = re.findall(r'\d+', rgb)
    rgb = [int(r) for r in rgb]

    return color_name, rgb

# Read raw file.
with open('colors_raw.txt') as f:
  content = f.readlines()

# Write cleaned up values back to file.
filename = 'colors_processed.txt'

with open(filename, 'w') as fh:
  for line in content:
    color_name, rgb = get_color_from_string(line)

    if color_name is not None:
      print("Raw color: {} (processed: {}, {})".format(line, color_name, rgb))
      fh.write("{{{}, {}, {}, \"{}\"}},\n".format(rgb[0], rgb[1], rgb[2], color_name))

print(get_color_from_string(content[0]))
