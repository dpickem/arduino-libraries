/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

// Include Pixel dependencies.
#include "callback_handler.h"

bool CallbackHandler::is_callback_registered(String name) {
  if ((get_callback_by_name(name) != nullptr)) {
      return true;
  }

  return false;
}

bool CallbackHandler::remove_callback(String name) {
  if (!is_callback_registered(name)) {
    // Buffer the list size before removal of an entry.
    int list_size_pre_removal = callbacks_.size();

    // Try to remove list element.
    callbacks_.remove(get_callback_index_by_name(name));

    // Return success iff the size of the list after removal is
    // strictly smaller than before.
    return callbacks_.size() > list_size_pre_removal;
  }

  LOG_VALUE("Failed to remove callback. Callback not found", name);
  return false;
}

bool CallbackHandler::add_named_callback(String name, Callback fcn) {
  if (!is_callback_registered(name)) {
    callbacks_.add({name, fcn});
    return true;
  }

  LOG_VALUE("Callback already registered", name);
  return false;
}

Callback CallbackHandler::get_callback_by_name(String name) {
  if (callbacks_.size() > 0) {
    // Find mode with specified name.
    for(int i = 0; i < callbacks_.size(); i++){
      if (callbacks_.get(i).name == name) {
        return callbacks_.get(i).fcn;
      }
    }
  }

  return nullptr;
}

int CallbackHandler::get_callback_index_by_name(String name) {
  if (callbacks_.size() > 0) {
    // Find mode with specified name.
    for(int i = 0; i < callbacks_.size(); i++){
      if (callbacks_.get(i).name == name) {
        return i;
      }
    }
  }

  return -1;
}
