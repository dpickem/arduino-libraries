/*
 * This is a library for the Atmel QTouch AT42QT2120
 * But it should work with other QTouch Devices as well
 *
 * This software is part of the firmware running Little Helper. You may use, copy, 
 * modify, merge, publish, distribute, sublicense, and/or sell copies of the Software 
 * but you must include this copyright notice and this permission in all copies or 
 * substantial portions of the Software.
 *
 * Appfruits invests time and resources to make this open source. Please support us
 * and the open-source hardware initiative.
 * 
 * Copyright 2014-2015 Phillip Schuster (@appfruits)
 * http://www.appfruits.com
 * 
 * MIT-License
 *
 * Modified 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 */

// Include QTouch header file.
#include "touch_wheel.h"

/**********************************************************
 *                  PUBLIC FUNCTIONS
 **********************************************************/

/*! @brief Class constructor for the AT42QT2120_QTOUCH
 *         delegate.
 *
 * @param driver Pointer to an instance of the
 *               AT42QT2120_QTOUCH driver.
 */
TouchWheel::TouchWheel(AT42QT2120_QTouch* driver) {
	this->_driver = driver;
	this->_driver->setDelegate(this);
	this->resetStates();
}

/*! @brief Initialize the QTouch delegate instance.
 *
 *  In particular this function calls the begin() function
 *  of the driver, enables the wheel mode, sets the
 *  detection thresholds as well as the detection
 *  integrator value.
 */
void TouchWheel::begin() {
	this->_driver->begin();
	this->_driver->enableWheel();
  setup();
}

void TouchWheel::begin(uint8_t sda_pin, uint8_t scl_pin) {
	this->_driver->begin(sda_pin, scl_pin);
	this->_driver->enableWheel();
  setup();
}

void TouchWheel::setup() {
	this->_driver->setDetectThreshold(0,41);
	this->_driver->setDetectThreshold(1,41);
	this->_driver->setDetectThreshold(2,41);
	this->_driver->setDetectionIntegrator(3);
}

/*! @brief Update the delegate's internal slider delta
 *         value with data from the driver.
 */
void TouchWheel::update() {
	this->resetStates();

	if (this->_driver->eventAvailable()) {
		if (this->_driver->wheelDetected()) {
			this->_sliderDelta = this->_driver->sliderDelta();
		}
	}
}

/*! @brief Reset the internal state of the delegate.
 *
 * Resets the button array, the slider delta and position
 * value.
 */
void TouchWheel::resetStates() {
	for (int i=0;i<4;i++) {
		this->_button[i] = 0;
	}

	this->_sliderDelta = 0;
  // TODO: Should this be deactivated?
  // this->_sliderPos = 0;
}

/*! @brief Determine whether a button has been released.
 *
 * @param direction The direction of the button to be
 *                  checked for release (left, right, top,
 *                  bottom).
 *
 * @return True if the specified button is released, false
 *         otherwise.
 */
bool TouchWheel::isButtonReleased(Direction direction) {
	return (this->_button[direction] > 0);
}

bool TouchWheel::isAnyButtonReleased() {
  for (uint8_t i = 0; i < N_BUTTONS; i++) {
    if (_button[i] > 0) {
      return true;
    }
  }

  return false;
}

/*! @brief Determine whether the driver is in sliding mode.
 *
 * @return True if the driver is in sliding mode, false
 *         otherwise.
 */
bool TouchWheel::isSliding() {
	return this->_sliding;
}

/*! @brief Retrieve the current slider delta.
 *
 * @return The current slider delta as integer value.
 */
int8_t TouchWheel::getSliderDelta() {
	return this->_sliderDelta;
}

/*! @brief Retrieve the current absolute slider position.
 *
 * @return The current slider position as unsigned integer
 *         value.
 */
uint8_t TouchWheel::getSliderPosition() {
	return this->_sliderPos;
}

/*! @brief Callback function for left button.
 *
 * Sets the internal state of the delegate for the left
 * button, i.e. sets the button press duration.
 *
 * @param duration The button press duration as
 *                 dt = t_buttonUp - t_buttonDown.
 */
void TouchWheel::onTouchLeftButtonPressed(uint32_t duration) {
	_button[Direction::Left] = duration;
  debugPrint("onTouchLeftButtonPressed", duration);
}

/*! @see TouchWheel::onTouchLeftButtonPressed */
void TouchWheel::onTouchTopButtonPressed(uint32_t duration) {
	_button[Direction::Top] = duration;
  debugPrint("onTouchTopButtonPressed", duration);
}

/*! @see TouchWheel::onTouchLeftButtonPressed */
void TouchWheel::onTouchRightButtonPressed(uint32_t duration) {
	_button[Direction::Right] = duration;
  debugPrint("onTouchRightButtonPressed", duration);
}

/*! @see TouchWheel::onTouchLeftButtonPressed */
void TouchWheel::onTouchBottomButtonPressed(uint32_t duration) {
	_button[Direction::Bottom] = duration;
  debugPrint("onTouchBottomButtonPressed", duration);
}

/*! @brief Callback function for slider mode.
 *
 * Sets the internal state of the delegate for the slider's
 * absolute position and change in position.
 *
 * @param sliderPos The absolute position of the slider.
 * @param sliderDelta The current change in slider position.
 */
void TouchWheel::onSlider(uint8_t sliderPos, int8_t sliderDelta) {
	this->_sliderPos = sliderPos;
	this->_sliderDelta = sliderDelta;
	this->_sliding = true;

#ifdef TOUCH_WHEEL_DEBUG
  Serial.print("onSlider called with sliderPos ");
  Serial.print(sliderPos);
  Serial.print(" and sliderDelta ");
  Serial.println(sliderDelta);
#endif
}

/*! @brief Callback function for button down function. */
void TouchWheel::onTouchDown() {
	this->_touching = true;
  debugPrint("onTouchDown", 0);
}

/*! @brief Callback function for button up function. */
void TouchWheel::onTouchUp(uint32_t duration) {
	this->_touching = false;
  debugPrint("onTouchUp", duration);
}

/*! @brief Helper function for debug prints for callback
 *         functions on serial interface.
 *
 * @param function_name The function name to be added to
 *                      the debug output.
 * @param duration The duration of the button press.
 */
void TouchWheel::debugPrint(String function_name, uint32_t duration) {
#ifdef TOUCH_WHEEL_DEBUG
  Serial.print(function_name);
  Serial.print(" called with duration ");
  Serial.println(duration);
#endif
}

