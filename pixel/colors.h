/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _COLORS_
#define _COLORS_

// Include Arduino libraries.
#include "Arduino.h"

// Include Pixel dependencies.
#include "color_palette.h"

class Color {
  public:
    // Default constructor.
    Color();

    // Constructor.
    Color(uint8_t r, uint8_t g, uint8_t b);
    Color(String color_name);

    // Destructor.
    ~Color() {};

    // Utility function.
    void print();

    // Getters.
    ColorDescriptor get_color_descriptor();
    String get_color_name();

    // Setters.
    void set_rgb(uint8_t r, uint8_t g, uint8_t b);

  private:
    ColorDescriptor color_;
};

#endif //_COLORS_
