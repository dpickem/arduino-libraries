/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */
#ifndef _PIXEL_BUTTON_H_
#define _PIXEL_BUTTON_H_

// Include Arduino libraries.
#include "Arduino.h"

#define BUTTON_PIN 4

/*! @brief A class providing an interface to a physical button.
 *
 */
class Button {
  public:
    // Constructor.
    Button(uint8_t pin,
           bool active_low = true,
           uint32_t debounce_time_ms = 50);

    // Destructor.
    ~Button() {};

    // Setup and update function.
    void update();

    // Mode detection functions.
    bool isButtonPressed();
    bool isButtonReleased();
    bool isButtonClicked();

    // Getter functions.
    uint32_t getClickDuration();
    uint32_t getButtonPressedTime();
    uint32_t getButtonPressedDuration();

  private:
    // Button parameters.
    bool      active_low_;
    uint8_t   pin_;
    uint32_t  debounce_time_ms_;

    // Timestamps and durations.
    uint32_t  button_press_start_ts_;
    uint32_t  button_press_duration_;

    // Buffer current button state.
    bool clicked_;
    bool last_state_;
};

#endif //_PIXEL_BUTTON_H_
