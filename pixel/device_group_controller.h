/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _DEVICE_GROUP_CONTROLLER_
#define _DEVICE_GROUP_CONTROLLER_

// Include Arduino libraries.
#include "Arduino.h"

// Include STL containers.
#include "LinkedList.h"

// Include Pixel dependencies.
#include "device_group.h"
#include "utilities.h"

/*! @brief A class representing a controller for managing device groups.
 *  
 */
class DeviceGroupController {
  public:
    // Constructor
    DeviceGroupController() : groups_() {
      // Initialize map iterator.
      current_group_ = 0;
    };

    // Destructor
    ~DeviceGroupController();

    // Device mode management.
    bool add_device_group(DeviceGroup* group);
    bool remove_device_group(String name);
    void print_device_group_details();

    // Getter functions.
    int get_number_of_device_groups();
    DeviceGroup* get_current_device_group();
    DeviceGroup* get_device_group_by_name(String name);
    DeviceGroup* get_device_group_by_index(uint8_t index);

    // Setter functions.
    bool set_next_device_group();
    bool set_previous_device_group();

  private:
    // Index pointing to current device group.
    int8_t current_group_;

    // List of device modes.
    LinkedList<DeviceGroup*> groups_;
};

#endif //_DEVICE_GROUP_CONTROLLER_
