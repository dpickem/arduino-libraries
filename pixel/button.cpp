/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#include "button.h"

Button::Button(uint8_t pin,
               bool active_low,
               uint32_t debounce_time_ms) {
  // Initialize pin mode for button pin.
  pin_ = pin;
  pinMode(pin_, INPUT);

  // Initialize button press timestamp.
  button_press_start_ts_ = 0;
  button_press_duration_ = 0;

  // Store button parameters.
  active_low_ = active_low;
  debounce_time_ms_ = debounce_time_ms;

  // Initialize last button state.
  last_state_ = active_low_;
  clicked_ = false;
}

void Button::update() {
  if (digitalRead(pin_) != last_state_) {
    Serial.print("Button state changed from ");
    Serial.print(last_state_);
    Serial.print(" to ");
    Serial.println(digitalRead(pin_));
  }

  // Buffer last state for output stability.
  last_state_ = digitalRead(pin_);

  // Reset clicked flag.
  clicked_ = false;

  // Button is pressed and was not pressed before.
  if (isButtonPressed() and button_press_start_ts_ == 0) {
    // Update timestamp, which indicates a button press event.
    button_press_start_ts_ = millis();
    Serial.print("Button pressed with ts: ");
    Serial.println(button_press_start_ts_);
  }

  // Button is released.
  if (isButtonReleased() and button_press_start_ts_ > 0) {
    button_press_duration_ = millis() - button_press_start_ts_;
    Serial.print("Button released at duration: ");
    Serial.println(button_press_duration_);

    // Debounce button.
    if (button_press_duration_ > debounce_time_ms_) {
      // Register a click.
      clicked_ = true;

      Serial.print("Button clicked at duration: ");
      Serial.println(button_press_duration_);
      Serial.println("----");

      // Reset timestamp.
      button_press_start_ts_ = 0;
    }
  }
}

bool Button::isButtonPressed() {
  /* Truth table -> XOR
   *
   * NOTE: A value of ActiveLow = True means that the combination
   * AL=True and LOW should be True.
   *
   *  AL  |  pin state | output
   *  -------------------------
   *   L        L          L
   *   L        H          H
   *   H        L          H
   *   H        H          L
   */
  return active_low_ != last_state_;
}

bool Button::isButtonReleased() {
  return active_low_ == last_state_;
}

bool Button::isButtonClicked() {
  return clicked_;
}

uint32_t Button::getClickDuration() {
  return button_press_duration_;
}

uint32_t Button::getButtonPressedTime() {
  return button_press_start_ts_;
}

uint32_t Button::getButtonPressedDuration() {
  if (button_press_start_ts_ > 0) {
    return millis() - button_press_start_ts_;
  }

  return 0;
}
