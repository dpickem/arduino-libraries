/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _PIXEL_CONFIG_
#define _PIXEL_CONFIG_

#include "enum.h"

// Define serial parameters.
#define SERIAL_BAUD 115200

// Define I2C parameters.
#define SDA_PIN 2
#define SCL_PIN 14

// Define RGB Led parameters.
#define GROUP_LED 0
#define MODE_LED 1
#define RGB_LEDS_PIN 4
#define RGB_LEDS_NUM 2
#define RGB_LEDS_BRIGHTNESS 128

// Touch sensor parameters.
// NOTE: On the NodeMCU board, use pin D7 (i.e. pin 13).
//       On a regular Arduino Nano, use pin 3.
#define TOUCH_CHANGE_PIN 12

// Button parameters.
#define BUTTON_PIN 4

// Define update rates.
#define UPDATE_DELAY_MS 10

// Define device UUID and name.
#define DEVICE_NAME "pixel_1"
#define DEVICE_UUID "B529C5FB-F9AB-462F-B3F3-509D8D9FD079"

// Define Wifi authentication.
#define WIFI_SSID "Y&D"
#define WIFI_PASSWORD "YT19DPcherry!!!"

// Define MQTT broker and authentication.
#define MQTT_BROKER_IP "192.168.0.116"
#define MQTT_USERNAME ""
#define MQTT_PASSWORD ""

// Define communication protocol parameters.
#define JSON_BUFFER_SIZE_MAX 1024

// Define topic to publish on.
namespace topics_publish {
  const String TOPIC_ERROR        = "/home/" + String(DEVICE_UUID) + "/error";
  const String TOPIC_SENSOR_DATA  = "/home/" + String(DEVICE_UUID) + "/sensor_data";
  const String TOPIC_USER_INPUT   = "/home/" + String(DEVICE_UUID) + "/user_input";
  const String TOPIC_CONFIG       = "/home/" + String(DEVICE_UUID) + "/config";
  const String TOPIC_RESPONSE     = "/home/" + String(DEVICE_UUID) + "/response";
  const String TOPIC_STATE        = "/home/" + String(DEVICE_UUID) + "/state";
}

// Define topic to subscribe to.
// NOTE: The subscription topics implicitly define the supported commands,
//       i.e. each topic implements its own callback that processes the
//       request received and responds with the requested data.
namespace topics_subscribe {
  // Get functions.
  const String TOPIC_GET_STATE  = "/home/" + String(DEVICE_UUID) + "/get_state";
  const String TOPIC_GET_DATA   = "/home/" + String(DEVICE_UUID) + "/get_data";
  const String TOPIC_GET_CONFIG = "/home/" + String(DEVICE_UUID) + "/get_config";
  // Set functions.
  const String TOPIC_SET_CONFIG = "/home/" + String(DEVICE_UUID) + "/set_config";
  // General request.
  const String TOPIC_REQUEST    = "/home/" + String(DEVICE_UUID) + "/request";
}

// Define animations, device modes, and groups.
struct DeviceModeInitializer {
  String name;
  DeviceModeType type;
  float min_val;
  float max_val;
  float step;
};

struct DeviceGroupInitializer {
  String name;
  String color;
  int device_modes[5];
};

const DeviceModeInitializer device_modes[] = {
  {"on_off", DeviceModeType::ON_OFF, 0, 1, 1},
  {"dimming", DeviceModeType::DIMMING, 0, 255, 5},
  {"color", DeviceModeType::COLOR, 0, 255, 5}
};

const DeviceGroupInitializer device_groups[] = {
  {"kitchen",         "red",    {0, 1, -1, -1, -1}},
  {"living_room",     "green",  {0, 1, 2, -1, -1}},
  {"bedroom",         "blue",   {0, 1, -1, -1, -1}},
  {"bedroom master",  "yellow", {0, 1, -1, -1, -1}},
  {"bathroom",        "orange", {0, 1, 2, -1, -1}},
  {"bathroom master", "brown",  {0, 1, 2, -1, -1}}
};

#endif //_PIXEL_CONFIG_
