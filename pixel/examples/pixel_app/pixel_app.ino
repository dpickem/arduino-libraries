/***************************************************************************
  This is the application sketch for the Pixel HMI device.

  Written by Daniel Pickem.

  MIT license, all text above must be included in any redistribution
 ***************************************************************************/
// Include Pixel dependencies.
#include "application.h"

// Instantiate Pixel App.
Application app;

void setup() {
  // Setup Pixel application.
  app.setup();
}

void loop() {
  app.loop();
}
