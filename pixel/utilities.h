/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _UTILITIES_
#define _UTILITIES_

#include "Arduino.h"

// Define macros in utility header for use in all files.
//#define LOG(m) if (Serial.available()) {Serial.println(m);}
//#define LOG_VALUE(m,v) if (Serial.available()) {Serial.print(m);Serial.print(": ");Serial.println(v);}

#define LOGNN(m) Serial.print(m);
#define LOG(m) Serial.println(m);
#define LOG_VALUE(m,v) Serial.print(m);Serial.print(": ");Serial.println(v);

#endif //_UTILITIES_
