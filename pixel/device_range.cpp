/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

// Include Arduino libraries.
#include "device_range.h"

DeviceRange::DeviceRange(float min_val,
                         float max_val,
                         float step) {
  // Initialize class variables.
  min_ = min_val;
  max_ = max_val;
  step_ = step;

  // Set initial range value to be at the minimum.
  current_ = min_val;
}

float DeviceRange::get_current() {
  return current_;
}

float DeviceRange::get_next() {
  // Increment current value capped at max_ value.
  current_ = min(current_ + step_,  max_);
  return current_;
}

float DeviceRange::get_previous() {
  // Decrement current value capped at min_ value.
  current_ = max(current_ - step_,  min_);
  return current_;
}

float DeviceRange::get_min() {
  return min_;
}

float DeviceRange::get_max() {
  return max_;
}

float DeviceRange::get_step() {
  return step_;
}

bool DeviceRange::is_at_max() {
  return current_ >= max_;
}

bool DeviceRange::is_at_min() {
  return current_ <= min_;
}

void DeviceRange::set_min(float min_val) {
  min_ = min_val;
}

void DeviceRange::set_to_min() {
  current_ = min_;
}

void DeviceRange::set_max(float max_val) {
  max_ = max_val;
}

void DeviceRange::set_to_max() {
  current_ = max_;
}

void DeviceRange::set_step(float step) {
  step_ = step;
}
