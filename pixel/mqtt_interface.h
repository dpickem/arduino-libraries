/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _MQTT_INTERFACE_
#define _MQTT_INTERFACE_

// Include Arduino library.
#include <Arduino.h>

// Include Wifi and MQTT libraries.
#include <ESP8266WiFi.h>
#include <MQTT.h>

// Include Pixel dependencies.
#include "callback_handler.h"
#include "config.h"
#include "error_codes.h"
#include "utilities.h"

class MQTTInterface{
  public:
    // Constructor.
    MQTTInterface() {};

    // Destructor.
    ~MQTTInterface() {};

    // Setup functions.
    static bool setup(bool connect_client=false);
    static bool connect();

    // Main loop.
    static void update();

    // Topic handling.
    bool register_topic_callback(String topic, Callback cb);
    bool unregister_topic(String topic);

    // Message handling.
    static void process_incoming_message(String& topic, String& message);

    // Error handling.
    static void error_handler(ErrorCode error_code, String error_msg_opt);

    // Callbacks.
    static void test_callback(ParsedJsonObject& obj);

  private:
    // Callback handler.
    static CallbackHandler cb_handler_;

    // Wifi client.
    static WiFiClient wifi_client_;

    // MQTT client.
    static MQTTClient mqtt_client_;
};

#endif //_MQTT_INTERFACE_
