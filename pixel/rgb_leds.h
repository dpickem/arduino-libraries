/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _RGB_LEDS_
#define _RGB_LEDS_

// Include Adafruit NeoPixel library.
#include <Adafruit_NeoPixel.h>

// Include utilities.
#include "colors.h"
#include"utilities.h"

class RGBLeds{
  public:
    // Constructor.
    RGBLeds(uint8_t number_of_leds,
            uint8_t led_pin,
            uint8_t brightness = 255);

    // Destructor.
    ~RGBLeds() {}

    // Initializer.
    void begin();
    void initialize();

    // Getters.
    uint8_t get_number_of_leds();
    uint8_t get_led_pin();

    // Setters.
    bool set_led_color(ColorDescriptor color,
                       int8_t led_index = -1,
                       uint8_t brightness = 255);

    bool set_led_color(uint8_t r,
                       uint8_t g,
                       uint8_t b,
                       int8_t led_index = -1,
                       uint8_t brightness = 255);
  public:
    Adafruit_NeoPixel leds_;

  private:
    uint8_t number_of_leds_;
    uint8_t led_pin_;
    uint8_t brightness_max_;
};


#endif //_RGB_LEDS_
