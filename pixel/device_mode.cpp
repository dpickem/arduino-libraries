/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

// Include Arduino libraries.
#include "device_mode.h"

// Utility functions.
void DeviceMode::increase() {
  auto next = range_.get_next();
  LOG_VALUE("Increased device mode value to", next);
}

void DeviceMode::decrease() {
  auto previous = range_.get_previous();
  LOG_VALUE("Decreased device mode value to", previous);
}

void DeviceMode::update_device_range(float min_val,
                                     float max_val,
                                     float step) {
  range_.set_min(min_val);
  range_.set_max(max_val);
  range_.set_step(step);

  LOG_VALUE("Setting values for device range", name_);
  LOG_VALUE("Setting device range min to", min_val);
  LOG_VALUE("Setting device range min to", max_val);
  LOG_VALUE("Setting device range min to", step);
}

// Getter function implementations.
String DeviceMode::get_name() {
  return name_;
}

DeviceModeType DeviceMode::get_type() {
  return type_;
}

float DeviceMode::get_current_state() {
  return range_.get_current();
}

DeviceRange* DeviceMode::get_device_range() {
  return &range_;
}

String DeviceMode::get_color() {
  if (type_ == DeviceModeType::COLOR) {
    return "green";
  } else {
    return "white";
  }
}

uint8_t DeviceMode::get_brightness() {
  if (type_ == DeviceModeType::ON_OFF) {
    return range_.get_current() * 255;
  } else {
    return range_.get_current();
  }
}

Animation* DeviceMode::get_animation() {
  return animation_;
}

String DeviceMode::get_animation_name() {
  if (animation_ != nullptr) {
    return animation_->get_name();
  }

  return "";
}

// Setter function implementations.
void DeviceMode::set_name(String name) {
  name_ = name;
}

void DeviceMode::set_type(DeviceModeType type) {
  type_ = type;
}

void DeviceMode::set_to_max() {
  range_.set_to_max();
}

void DeviceMode::set_to_min() {
  range_.set_to_min();
}

void DeviceMode::set_animation(Animation* animation) {
  animation_ = animation;
}

// Debug output functions.
void DeviceMode::print_device_mode_details() {
  Serial.println("------------------------------------");
  Serial.print("Device mode name: ");
  Serial.println(name_);
  Serial.print("   Type: ");
  Serial.println(get_device_mode_type_string(static_cast<int>(type_)));
  Serial.print("   Range: ");
  Serial.print(range_.get_min());
  Serial.print(" : ");
  Serial.print(range_.get_step());
  Serial.print(" : ");
  Serial.println(range_.get_max());
  Serial.print("   Current: ");
  Serial.println(range_.get_current());
  Serial.println("------------------------------------");
}
