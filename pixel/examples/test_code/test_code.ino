/***************************************************************************
  This is a library for the AT42QT2120 capacitive touch sensor.

  This sensor uses I2C to communicate, at a minimum 2 pins are required
  to interface.

  Written by Daniel Pickem.

  MIT license, all text above must be included in any redistribution
 ***************************************************************************/
// Include Pixel dependencies.
#include "application.h"

// Instantiate RGB leds.
Application app;

DeviceRange dr(0,10,1);
DeviceMode dm("test_mode_1", DeviceModeType::ON_OFF, 0, 1, 1);
DeviceMode dm1("test_mode_2", DeviceModeType::ON_OFF, 0, 1, 1);
DeviceGroup dg("test_group", "blue");


void setup() {
  // Setup Pixel application.
  app.setup();

  Serial.print("Device group name: ");
  Serial.println(dg.get_name());

  Serial.print("Get Device group name for empty group: ");
  auto ret_val = dg.get_device_mode_by_name(String("nonexistent"));
  if (ret_val == nullptr) {
    Serial.println("nullptr returned");
  }

  Serial.print("Add new device mode: ");
  Serial.println(dg.add_device_mode(&dm));
  Serial.print("Add new device mode: ");
  Serial.println(dg.add_device_mode(&dm1));

  Serial.print("Get Device group name for non-empty group: ");
  auto ret_val_n = dg.get_device_mode_by_name(String("test_mode_1"));
  if (ret_val_n == nullptr) {
    Serial.println("nullptr returned");
  } else {
    Serial.println(ret_val_n->get_name());
  }
  Serial.print("Current device mode name: ");
  Serial.println(dg.get_current_device_mode()->get_name());

  // Increase device mode in device group.
  Serial.print("Increase device mode: ");
  Serial.println(dg.increase_current_device_mode());

  Serial.print("Current device mode name: ");
  Serial.println(dg.get_current_device_mode()->get_name());

  // Decrease device mode in device group.
  Serial.print("Decrease device mode: ");
  Serial.println(dg.decrease_current_device_mode());

  Serial.print("Current device mode name: ");
  Serial.println(dg.get_current_device_mode()->get_name());
  
  Serial.print("Min device range: ");
  Serial.println(dr.get_min());
  Serial.print("Min device range: ");
  Serial.println(dm.get_device_range()->get_min());

  Serial.print("Current device range value: ");
  Serial.println(dr.get_current());
  dr.get_next();
  Serial.print("Current device range value: ");
  Serial.println(dr.get_current());
  dr.get_next();
  Serial.print("Current device range value: ");
  Serial.println(dr.get_current());
}

void loop() {
  app.loop();
}
