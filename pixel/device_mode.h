/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _DEVICE_MODE_
#define _DEVICE_MODE_

// Include Arduino libraries.
#include "Arduino.h"

// Include Pixel dependencies.
#include "animation.h"
#include "colors.h"
#include "device_range.h"
#include "enum.h"
#include "utilities.h"

/*! @brief A class representing a device mode.
 *
 * NOTE: No set_device_range function is provided because it can't be
 *       guaranteed that the provided device range has the same template
 *       type as the range stored in the DeviceMode class.
 */
class DeviceMode {
  public:
    // Constructor
    DeviceMode(String name,
               DeviceModeType type,
               float min_val,
               float max_val,
               float step,
               Animation* animation = nullptr)
      : range_(min_val, max_val, step) {
      //Initialize class variables that are not covered by initializer.
      name_ = name;
      type_ = type;

      // Set animation pointer.
      animation_ = animation;
    };

    // Destructor
    ~DeviceMode() {
      // Destroy animation pointer.
      if (animation_ != nullptr) {
        delete animation_;
      }
    };

    // Utility functions.
    void increase();
    void decrease();
    void update_device_range(float min_val,
                             float max_val,
                             float step);

    // Getter functions.
    String get_name();
    DeviceModeType get_type();
    float get_current_state();
    Animation* get_animation();
    String get_animation_name();

    // Getter function returning a reference.
    DeviceRange* get_device_range();

    // Getter functions for color and brightness.
    String get_color();
    uint8_t get_brightness();

    // Setter functions.
    void set_name(String name);
    void set_type(DeviceModeType type);
    void set_to_max();
    void set_to_min();
    void set_animation(Animation* animation);

    // Debug output functions.
    void print_device_mode_details();

  private:
    // Name of the device mode variables.
    String name_;

    // Type of mode.
    DeviceModeType type_;

    // Numeric range of values the mode can accept.
    DeviceRange range_;

    // Animation associated with mode.
    Animation* animation_ = nullptr;
};

#endif //_DEVICE_RANGE_
