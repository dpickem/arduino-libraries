/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _ERROR_CODES_
#define _ERROR_CODES_

#include "Arduino.h"

enum class ErrorCode : int {
  MQTT_CALLBACK_SUCCESSFULLY_REGISTERED,
  ERROR_MQTT_CALLBACK_ALREADY_REGISTERED,
  ERROR_MQTT_CALLBACK_NOT_FOUND,
  ERROR_INPUT_STRING_EXCEEDS_MAX_LENGTH,
  ERROR_JSON_PARSING_FAILED,
  ERROR_PARAMETER_MISSING,
  ERROR_PARAMETER_INVALID_DATATYPE,
  ERROR_PARAMETER_INVALID_VALUE,
};

static const char * ErrorCodesStrings[] PROGMEM = {
  "MQTT_CALLBACK_SUCCESSFULLY_REGISTERED",
  "ERROR_MQTT_CALLBACK_ALREADY_REGISTERED",
  "ERROR_MQTT_CALLBACK_NOT_FOUND",
  "ERROR_INPUT_STRING_EXCEEDS_MAX_LENGTH",
  "ERROR_JSON_PARSING_FAILED",
  "ERROR_PARAMETER_MISSING",
  "ERROR_PARAMETER_INVALID_DATATYPE",
  "ERROR_PARAMETER_INVALID_VALUE"
};

// Enum to string mapping functions.
String get_error_code_string(ErrorCode enumVal);

#endif //_ERROR_CODES_
