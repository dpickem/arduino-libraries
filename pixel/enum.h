/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _PIXEL_ENUM_
#define _PIXEL_ENUM_

// Include Arduino libraries.
#include "Arduino.h"

enum class DeviceModeType : int {
  ON_OFF,
  DIMMING,
  COLOR
};

static const char * DeviceModeTypeStrings[] = {
  "ON_OFF",
  "DIMMING",
  "COLOR"
};

enum class DeviceGroupType {
  LIGHTING,
  OUTLETS,
  SENSORS 
};

static const char * DeviceGroupTypeStrings[] = {
  "LIGHTING",
  "OUTLETS",
  "SENSORS"
};

// Enum to string mapping functions.
String get_device_mode_type_string(int enumVal);
String get_device_group_type_string(int enumVal);

#endif //_PIXEL_ENUM_
