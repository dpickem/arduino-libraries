/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#include "application.h"

// Initialize all static variables of the Application class.
DeviceGroupController Application::device_group_controller_ = DeviceGroupController();
MQTTInterface Application::mqtt_interface_ = MQTTInterface();

void Application::setup() {
  // Setup serial.
  Serial.begin(SERIAL_BAUD);

  // Initialize LEDs and set color to white and brightness to 0.
  leds_.begin();
  leds_.initialize();

  // Initialize touch wheel.
  touch_wheel_.begin(SDA_PIN, SCL_PIN);

  // Set up MQTT client and topic subscriptions.
  setup_mqtt();

  // Add all device groups defined in config file.
  setup_device_groups();
}

void Application::loop() {
  // Update MQTT communication.
  mqtt_interface_.update();

  // Update button.
  process_button_actions();

  // Update touch wheel.
  process_touch_wheel_actions();

  // Update LEDs.
  update_leds();  

  // Update with fixed upate rate (100 Hz).
  delay(UPDATE_DELAY_MS);
}

void Application::setup_mqtt() {
  // Initialize and connect WiFi and MQTT client.
  mqtt_interface_.setup(true);

  // Register MQTT callbacks.
  mqtt_interface_.register_topic_callback(topics_subscribe::TOPIC_GET_STATE,
                                          Application::callback_get_state);
  mqtt_interface_.register_topic_callback(topics_subscribe::TOPIC_GET_DATA,
                                          Application::callback_get_data);
  mqtt_interface_.register_topic_callback(topics_subscribe::TOPIC_GET_CONFIG,
                                          Application::callback_get_config);
  mqtt_interface_.register_topic_callback(topics_subscribe::TOPIC_SET_CONFIG,
                                          Application::callback_set_config);
  mqtt_interface_.register_topic_callback(topics_subscribe::TOPIC_REQUEST,
                                          Application::callback_request);
}

void Application::setup_device_groups() {
  for (const auto dgi : device_groups) {
    // Add new device group.
    device_group_controller_.add_device_group(new DeviceGroup(dgi.name, 
                                                              dgi.color));

    // Get device group that was just added.
    auto current_group = device_group_controller_.get_device_group_by_name(dgi.name);

    // Add all specified modes to device group.
    for (const int device_mode_index : dgi.device_modes) {
      if (device_mode_index >= 0) {
        // Retrieve device mode from config array.
        auto dmi = device_modes[device_mode_index];

        // Add device mode to current group.
        current_group->add_device_mode(new DeviceMode(dmi.name,
                                                      dmi.type,
                                                      dmi.min_val,
                                                      dmi.max_val,
                                                      dmi.step));
      }
    }

    // Debug print details of newly added device group.
    //current_group->print_device_group_details();
  }
}

void Application::process_touch_wheel_actions() {
  // Update touch wheel.
  touch_wheel_.update();

  // Reset flags depending on which button was pressed.
  update_group_led_ = touch_wheel_.isButtonReleased(Direction::Left) || 
                        touch_wheel_.isButtonReleased(Direction::Right);

  update_mode_led_ = touch_wheel_.isButtonReleased(Direction::Top) || 
                      touch_wheel_.isButtonReleased(Direction::Bottom) ||
                      touch_wheel_.getSliderDelta() != 0;

  // Up and down select device modes of current device group.
  if (touch_wheel_.isButtonReleased(Direction::Top)) {
    // Up sets current group's device mode to next mode.
    device_group_controller_.get_current_device_group()->set_next_device_mode();
  }

  if (touch_wheel_.isButtonReleased(Direction::Bottom)) {
    // Down sets current group's device mode to previous mode.
    device_group_controller_.get_current_device_group()->set_previous_device_mode();
  }

  // Left and right select device groups.
  if (touch_wheel_.isButtonReleased(Direction::Left)) {
    // Left sets previous device group.
    device_group_controller_.set_previous_device_group();  
  }

  if (touch_wheel_.isButtonReleased(Direction::Right)) {
    // Right sets next device group.
    device_group_controller_.set_next_device_group();  
  }

  // Process slider delta.
  int8_t touch_wheel_delta = touch_wheel_.getSliderDelta();
  if (touch_wheel_delta != 0) {
    if (touch_wheel_delta > 0) {
      // Positive delta increases current group's current mode's range value.
      device_group_controller_.get_current_device_group()->get_current_device_mode()->increase();
    } else {
      // Negative delta decreases current group's current mode's range value.
      device_group_controller_.get_current_device_group()->get_current_device_mode()->decrease();
    }
  }
}

void Application::process_button_actions() {
  // Update button.
  button_.update();

  // Process input actions.
  if (button_.isButtonClicked()) {
    // TODO: Implement this behavior.
  }
}

void Application::update_leds() {
  // Update group led.
  if (update_group_led_) {
    // Get const pointer to current group.
    const auto current_group = device_group_controller_.get_current_device_group();

    // Debug print device group details.
    current_group->print_device_group_details();

    // Update LED color.
    Color color = Color(current_group->get_color());
    leds_.set_led_color(color.get_color_descriptor(), GROUP_LED, 255);
  }

  // Update mode LED.
  if (update_mode_led_) {
    // Get const pointer to current group and mode.
    const auto current_group = device_group_controller_.get_current_device_group();
    const auto current_mode = current_group->get_current_device_mode();

    // Debug print device mode details.
    current_group->get_current_device_mode()->print_device_mode_details();

    // Get color and brightness from device mode.
    Color color = Color(current_mode->get_color());
    uint8_t brightness = current_mode->get_brightness();

    Serial.print("Setting brightness to: ");
    Serial.println(brightness);

    // Set LED color.
    leds_.set_led_color(color.get_color_descriptor(), MODE_LED, brightness);

    // TODO: Update device mode led via animation.
    // TODO: If the animation is not finished, update animation.
    // TODO: Enforce a max time limit for an animation of 1 sec.
    // TODO: Once animation is done, update mode LED according to mode state.
    // leds_.set_led_color(cd.r, cd.g, cd.b, MODE_LED, 255);
  }
}

// MQTT callback functions.
void Application::callback_request(ParsedJsonObject& json_object) {
  // Check if required key 'request' exists in JSON object.
  String request = json_object.root()["request"];

  if (request) {
    Serial.print("Requested command: ");
    Serial.println(request);
  }

  // Debug print configuration as JSON object.
  get_config_json_string(&device_group_controller_);
}

void Application::callback_get_state(ParsedJsonObject& obj) {}
void Application::callback_get_data(ParsedJsonObject& obj) {}
void Application::callback_get_config(ParsedJsonObject& obj) {}
void Application::callback_set_config(ParsedJsonObject& obj) {}
