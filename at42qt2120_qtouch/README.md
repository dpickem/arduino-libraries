This is a library for the Atmel QTouch AT42QT2120 capacitive touch sensor.

Unfortunately there is no off-the-shelf breakout board available for this
sensor. However, a breakout board for the TSSOP-20 package that this Atmel
sensor uses can be purchased from Adafruit at:

 * https//www.adafruit.com/product/1208

Check out the links below for tutorials and wiring diagrams:

 * Wiring diagram: Based on Appfruits LittleHelper available at https://github.com/appfruits/LittleHelper/tree/master/Hardware
 * Datasheet: http://ww1.microchip.com/downloads/en/DeviceDoc/doc9634.pdf

This library is based on work by Appfruits, which can be found at:

 * http://www.appfruits.com/2015/12/little-helper/
 * https://github.com/appfruits/LittleHelper/blob/master/Firmware/Appfruits_QTouch.h
 * https://github.com/appfruits/LittleHelper/blob/master/Firmware/Appfruits_QTouch.cpp
