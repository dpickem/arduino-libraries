/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

#ifndef _JSON_PARSING_
#define _JSON_PARSING_

// Include Arduino library.
#include <Arduino.h>

// Include ArduinoJson library.
#include <ArduinoJson.h>

/*! @brief A class facilitating the parsing and handling of JSON
 *         strings, which are the main data exchange format between
 *         the Pixel and the home network.
 */
class ParsedJsonObject{
  public:
    // Constructor.
    ParsedJsonObject(String json_string) : buffer_(json_string.length()),
                                     root_(buffer_.parseObject(json_string)) {
      // NOTE: The dynamic buffer is allocated in the initializer
      //       list and the JSON string is parsed there as well.
      //       The constructor itself only checks if the
      //       initialization and parsing was successful.

      // Check if parsing succeeded.
      is_valid_ = true;

      if (!root_.success()) {
        is_valid_ = false;
      }
    }

    // Destructor.
    ~ParsedJsonObject() {
      // TODO: Destroy root object and buffer. 
      // TODO: Since buffer_ and root_ are destroyed once out of scope, does
      //       a JsonParser object need an explicit destructor?
    }

    // Validation functions.
    bool is_valid() { return is_valid_; }

    // Getter functions.
    JsonObject& root() { return root_; }

    // Debug functions.
    void print() {
      for (JsonPair& p : root_) {
        Serial.print("key - value: ");
        Serial.print(p.key);
        Serial.print(" - ");
        Serial.println(String(p.value.as<char*>()));
      }
    }

    void pretty_print() {
      root_.prettyPrintTo(Serial);
    }

  private:
    DynamicJsonBuffer   buffer_;
    JsonObject&         root_;
    bool                is_valid_;
};

#endif //_JSON_PARSING_
