/*
 * This is a library for the Pixel v2.0 HMI touch device.
 *
 * This software is part of the firmware running the Pixel v2.0.
 * You may use, copy, modify, merge, publish, distribute,
 * sublicense, and/or sell copies of the Software but you must
 * include this copyright notice and this permission in all
 * copies or substantial portions of the Software.
 *
 * Copyright 2018 Daniel Pickem (daniel.pickem@gmail.com)
 * http://www.danielpickem.com
 * 
 * MIT-License
 *
 */

// Include Arduino libraries.
#include "animation.h"

AnimationBlink::AnimationBlink(Color color_on,
                               Color color_off,
                               uint32_t on_time_ms,
                               uint32_t off_time_ms,
                               uint8_t brightness,
                               uint32_t repetitions) {

  // Buffer input arguments.
  color_on_ = color_on;
  color_off_ = color_off;
  on_time_ms_ = on_time_ms;
  off_time_ms_ = off_time_ms;
  brightness_ = brightness;
  repetitions_ = repetitions;

  // Initialize state.
  on_ = false;
  color_current_ = color_off_;

  // Initialize transition count and timestamp.
  repetition_count_ = 0;
  last_transition_time_ms_= millis();
}

void AnimationBlink::reset() {
  // Set current state and color.
  on_ = false; 
  color_current_ = color_off_;

  // Reset repetition count.
  repetition_count_ = 0;

  // Reset transition timestamp.
  last_transition_time_ms_= millis();
}

void AnimationBlink::update() {
  // Only update if the animation is not yet finised.
  if (!is_finished()) {
    // Determine if a transition should happen.
    if (on_) {
      // Transition from ON -> OFF if the timer has expired.
      if (millis() - last_transition_time_ms_ > on_time_ms_) {
        // Set current state and color.
        on_ = false;
        color_current_ = color_off_;

        // Update repetition_count_.
        repetition_count_++;
      }
    } else {
      // Transition from OFF -> ON if the timer has expired.
      if (millis() - last_transition_time_ms_ > off_time_ms_) {
        // Set current state and color.
        on_ = true;
        color_current_ = color_off_;
      }
    }
  }
}

bool AnimationBlink::is_finished() {
  return repetitions_ > 0 && repetition_count_ == repetitions_;
}

Color AnimationBlink::get_current_color() {
  return color_current_;
}

String AnimationBlink::get_name() {
  return "Blink";
}
